#!/usr/bin/perl

my %subst = ();

for my $symfile (@ARGV) {
	read_symfile($symfile);
}

while (<STDIN>) {
	s/`([^`]+)`/$subst{$1}/ge;
	print;
}

sub read_symfile {
	my $fname = shift;
	open(my $in, "<", $fname) or die "$!";
	while (<$in>) {
		chomp;
		if (/^(\w+)\s+equ\s+(\d+)$/i) {
			$subst{$1} = $2;
		}
	}
	close $in;
}
