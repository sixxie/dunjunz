; Dunjunz - remade for the Dragon 32/64
; Copyright (c) 2017-2018 Ciaran Anscomb
;
; See COPYING file for redistribution conditions.

; Boot block, compatible with both Dragon and Tandy CoCo.  Copies a run of
; tokenised BASIC into low RAM, sets up the requisite pointers for BASIC
; to consider them as the next commands to execute, then returns to the
; OS.
;
; DragonDOS loads this from Track 0, Sector 3.  RSDOS loads it from Track
; 34, Sector 1.

		org $2600
		setdp 0		; assumed

boot_flag	fcc /OS/

boot_exec	lda #$08
		sta $ff22

		; compressing title screen more than makes up for having a
		; single-use copy of the dunzip code here...

dunzip		ldx #title0_dz
		ldu #$0400
dunz_loop	ldd ,x++
		bpl dunz_run	; run of 1-128 bytes
		tstb
		bpl dunz_7_7
dunz_14_8	lslb		; drop top bit of byte 2
		asra
		rorb		; asrd
		leay d,u
		ldb ,x+
		bra 10F		; copy 1-256 bytes (0 == 256)
dunz_7_7	leay a,u	; copy 1-128 bytes
10		lda ,y+
		sta ,u+
		incb
		bvc 10B		; count UP until B == 128
		bra 80F
1		ldb ,x+
dunz_run	stb ,u+
		inca
		bvc 1B		; count UP until B == 128
zdata_end	equ *+1
80		cmpx #title0_dz_end
		blo dunz_loop

		lda $a000	; [$A000] points to ROM1 in CoCos
		anda #$20
		beq 10F		; dragon
		dec run_token	; coco RUN token is $8e, not $8f
10		ldx #basic_next
		ldu #$02dc
		stu $00a6
20		lda ,x+
		sta ,u+
		bne 20B
		clr boot_flag	; needed for coco
		; work around DOSplus issue where BOOT does not
		; expect to return
		ldx ,s
		ldd 1,x
		cmpd #$8344	; looks like jump to error handler?
		bne 30F
		leas 2,s
30		rts

basic_next	fcc /:/
run_token	fcb $8f
		fcc /"DUNJUNZ":/,0

title0_dz
		includebin "title0.bin.dz"
title0_dz_end

		; fill rest of sector
		align 256,$09
