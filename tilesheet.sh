#!/bin/bash

# Generate graphics data from supplied image.

. sheetfuncs.sh

sheet="tiles.png"

test -x "$TILE2BIN" || { echo "$TILE2BIN: not executable" >&2; exit 1; }

W=12
H=9

# Everything between here and 'tiles_a_end' are unshifted tiles (set A).  INIT
# code generates the shifted set B from these.

label "tiles_a_start"

echo
echo "; player/monster graphics"

bitmap_01_a "$sheet" tile_p1_up    $((W*0)) $((H*0))
bitmap_01_a "$sheet" tile_p1_down  $((W*0)) $((H*0)) -X -Y
bitmap_01_a "$sheet" tile_p1_left  $((W*2)) $((H*0))
bitmap_01_a "$sheet" tile_p1_right $((W*2)) $((H*0)) -X -Y

bitmap_01_a "$sheet" tile_p2_up    $((W*0)) $((H*1))
bitmap_01_a "$sheet" tile_p2_down  $((W*0)) $((H*1)) -X -Y
bitmap_01_a "$sheet" tile_p2_left  $((W*2)) $((H*1))
bitmap_01_a "$sheet" tile_p2_right $((W*2)) $((H*1)) -X -Y

bitmap_01_a "$sheet" tile_p3_up    $((W*0)) $((H*2))
bitmap_01_a "$sheet" tile_p3_down  $((W*0)) $((H*2)) -X -Y
bitmap_01_a "$sheet" tile_p3_left  $((W*2)) $((H*2))
bitmap_01_a "$sheet" tile_p3_right $((W*2)) $((H*2)) -X -Y

bitmap_01_a "$sheet" tile_p4_up    $((W*0)) $((H*3))
bitmap_01_a "$sheet" tile_p4_down  $((W*0)) $((H*3)) -X -Y
bitmap_01_a "$sheet" tile_p4_left  $((W*2)) $((H*3))
bitmap_01_a "$sheet" tile_p4_right $((W*2)) $((H*3)) -X -Y

bitmap_01_a "$sheet" tile_monster_up    $((W*0)) $((H*4))
bitmap_01_a "$sheet" tile_monster_down  $((W*0)) $((H*4)) -X -Y
bitmap_01_a "$sheet" tile_monster_left  $((W*2)) $((H*4))
bitmap_01_a "$sheet" tile_monster_right $((W*2)) $((H*4)) -X -Y

echo
echo "; player shots"

bitmap_a "$sheet" tile_arrow_up    $((W*0)) $((H*5))
bitmap_a "$sheet" tile_arrow_down  $((W*0)) $((H*5)) -X -Y
bitmap_a "$sheet" tile_arrow_left  $((W*1)) $((H*5))
bitmap_a "$sheet" tile_arrow_right $((W*1)) $((H*5)) -X -Y

bitmap_a "$sheet" tile_fball_up    $((W*2)) $((H*5))
bitmap_a "$sheet" tile_fball_down  $((W*2)) $((H*5)) -X -Y
bitmap_a "$sheet" tile_fball_left  $((W*3)) $((H*5))
bitmap_a "$sheet" tile_fball_right $((W*3)) $((H*5)) -X -Y

bitmap_a "$sheet" tile_axe_up      $((W*0)) $((H*6))
bitmap_a "$sheet" tile_axe_down    $((W*0)) $((H*6)) -X -Y
bitmap_a "$sheet" tile_axe_left    $((W*1)) $((H*6))
bitmap_a "$sheet" tile_axe_right   $((W*1)) $((H*6)) -X -Y

bitmap_a "$sheet" tile_sword_up    $((W*2)) $((H*6))
bitmap_a "$sheet" tile_sword_down  $((W*2)) $((H*6)) -X -Y
bitmap_a "$sheet" tile_sword_left  $((W*3)) $((H*6))
bitmap_a "$sheet" tile_sword_right $((W*3)) $((H*6)) -X -Y

echo
echo "; bones - death animation"

bitmap_a "$sheet" tile_bones0      $((W*0)) $((H*7))
bitmap_a "$sheet" tile_bones1      $((W*1)) $((H*7))
bitmap_a "$sheet" tile_bones2      $((W*2)) $((H*7))
bitmap_a "$sheet" tile_bones3      $((W*3)) $((H*7))

bitmap_a "$sheet" tile_bones4      $((W*0)) $((H*8))
bitmap_a "$sheet" tile_bones5      $((W*1)) $((H*8))
bitmap_a "$sheet" tile_bones6      $((W*2)) $((H*8))
bitmap_a "$sheet" tile_bones7      $((W*3)) $((H*8))

echo
echo "; exit animations"

bitmap_a "$sheet" tile_p1_exit4 $((W*0)) $((H*9))
bitmap_a "$sheet" tile_p1_exit5 $((W*1)) $((H*9))
bitmap_a "$sheet" tile_p1_exit6 $((W*0)) $((H*9)) -X -Y
bitmap_a "$sheet" tile_p1_exit7 $((W*1)) $((H*9)) -X -Y

bitmap_a "$sheet" tile_p2_exit4 $((W*2)) $((H*9))
bitmap_a "$sheet" tile_p2_exit5 $((W*3)) $((H*9))
bitmap_a "$sheet" tile_p2_exit6 $((W*2)) $((H*9)) -X -Y
bitmap_a "$sheet" tile_p2_exit7 $((W*3)) $((H*9)) -X -Y

bitmap_a "$sheet" tile_p3_exit4 $((W*0)) $((H*10))
bitmap_a "$sheet" tile_p3_exit5 $((W*1)) $((H*10))
bitmap_a "$sheet" tile_p3_exit6 $((W*0)) $((H*10)) -X -Y
bitmap_a "$sheet" tile_p3_exit7 $((W*1)) $((H*10)) -X -Y

bitmap_a "$sheet" tile_p4_exit4 $((W*2)) $((H*10))
bitmap_a "$sheet" tile_p4_exit5 $((W*3)) $((H*10))
bitmap_a "$sheet" tile_p4_exit6 $((W*2)) $((H*10)) -X -Y
bitmap_a "$sheet" tile_p4_exit7 $((W*3)) $((H*10)) -X -Y

bitmap_a "$sheet" tile_exit8 $((W*0)) $((H*11))
bitmap_a "$sheet" tile_exit9 $((W*1)) $((H*11))
bitmap_a "$sheet" tile_exit10 $((W*0)) $((H*11)) -X -Y
bitmap_a "$sheet" tile_exit11 $((W*1)) $((H*11)) -X -Y

echo
echo "; misc items"

bitmap_a "$sheet" tile_floor    $((W*4)) $((H*0))
bitmap_a "$sheet" tile_exit     $((W*5)) $((H*0))
bitmap_a "$sheet" tile_trapdoor $((W*6)) $((H*0))
bitmap_a "$sheet" tile_tport    $((W*7)) $((H*0))

bitmap_a "$sheet" tile_drainer0 $((W*4)) $((H*1))
bitmap_a "$sheet" tile_drainer1 $((W*5)) $((H*1))
bitmap_a "$sheet" tile_money0   $((W*6)) $((H*1))
bitmap_a "$sheet" tile_money1   $((W*7)) $((H*1))

bitmap_a "$sheet" tile_power0   $((W*4)) $((H*2))
bitmap_a "$sheet" tile_power1   $((W*5)) $((H*2))
bitmap_a "$sheet" tile_weapon   $((W*6)) $((H*2))
bitmap_a "$sheet" tile_armour   $((W*7)) $((H*2))

bitmap_a "$sheet" tile_food     $((W*4)) $((H*3))
bitmap_a "$sheet" tile_potion   $((W*5)) $((H*3))
bitmap_a "$sheet" tile_cross    $((W*6)) $((H*3))
bitmap_a "$sheet" tile_speed    $((W*7)) $((H*3))

bitmap_a "$sheet" tile_door_v   $((W*5)) $((H*4))
bitmap_a "$sheet" tile_door_h   $((W*6)) $((H*4))
bitmap_a "$sheet" tile_key      $((W*7)) $((H*4))
bitmap_a "$sheet" tile_door1_v  $((W*5)) $((H*11))
bitmap_a "$sheet" tile_door1_h  $((W*6)) $((H*11))
bitmap_a "$sheet" tile_key1     $((W*7)) $((H*11))

echo
echo "; stones"

bitmap_a "$sheet" tile_stone01 $((W*4)) $((H*5))
bitmap_a "$sheet" tile_stone02 $((W*5)) $((H*5))
bitmap_a "$sheet" tile_stone03 $((W*6)) $((H*5))
bitmap_a "$sheet" tile_stone04 $((W*7)) $((H*5))

bitmap_a "$sheet" tile_stone05 $((W*4)) $((H*6))
bitmap_a "$sheet" tile_stone06 $((W*5)) $((H*6))
bitmap_a "$sheet" tile_stone07 $((W*6)) $((H*6))
bitmap_a "$sheet" tile_stone08 $((W*7)) $((H*6))

bitmap_a "$sheet" tile_stone09 $((W*4)) $((H*7))
bitmap_a "$sheet" tile_stone10 $((W*5)) $((H*7))
bitmap_a "$sheet" tile_stone11 $((W*6)) $((H*7))
bitmap_a "$sheet" tile_stone12 $((W*7)) $((H*7))

bitmap_a "$sheet" tile_stone13 $((W*4)) $((H*8))
bitmap_a "$sheet" tile_stone14 $((W*5)) $((H*8))
bitmap_a "$sheet" tile_stone15 $((W*6)) $((H*8))
bitmap_a "$sheet" tile_stone16 $((W*7)) $((H*8))

bitmap_a "$sheet" tile_stone17 $((W*4)) $((H*9))
bitmap_a "$sheet" tile_stone18 $((W*5)) $((H*9))
bitmap_a "$sheet" tile_stone19 $((W*6)) $((H*9))
bitmap_a "$sheet" tile_stone20 $((W*7)) $((H*9))

bitmap_a "$sheet" tile_stone21 $((W*4)) $((H*10))
bitmap_a "$sheet" tile_stone22 $((W*5)) $((H*10))
bitmap_a "$sheet" tile_stone23 $((W*6)) $((H*10))
bitmap_a "$sheet" tile_stone24 $((W*7)) $((H*10))

bitmap_a "$sheet" tile_stone25 $((W*4)) $((H*11))

echo
echo "tiles_a_end"

# Chalice graphics only ever appear in one position, and so shifted versions
# are not required.

echo
echo "; chalice"

bitmap_a "$sheet" tile_chalice_nw0 $((W*2)) $((H*11))
bitmap_b "$sheet" tile_chalice_ne0 $((W*2)) $((H*11)) -X
bitmap_a "$sheet" tile_chalice_sw0 $((W*2)) $((H*11)) -Y
bitmap_b "$sheet" tile_chalice_se0 $((W*2)) $((H*11)) -X -Y

bitmap_a "$sheet" tile_chalice_nw1 $((W*3)) $((H*11))
bitmap_b "$sheet" tile_chalice_ne1 $((W*3)) $((H*11)) -X
bitmap_a "$sheet" tile_chalice_sw1 $((W*3)) $((H*11)) -Y
bitmap_b "$sheet" tile_chalice_se1 $((W*3)) $((H*11)) -X -Y

echo
echo "chalice_end"

# Ditto tick and cross

echo
echo "; yes/no (tick/cross)"

bitmap_b "$sheet" tile_yes $((W*0)) $((H*12))
bitmap_b "$sheet" tile_no  $((W*1)) $((H*12))
