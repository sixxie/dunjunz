#!/bin/bash

PATH=/bin:/usr/bin
export PATH

. sheetfuncs.sh

label "death_screen"
cursor 28 8
encode_file "death-screen.txt"

label "end_screen"
cursor 28 8
encode_file "end-screen.txt"

label "credits_screen"
cursor 28 8
encode_file "credits-screen.txt"

label "credits_screen2"
cursor 30 8
encode_file "credits-screen2.txt"

label "text_logo"
encode_string "~"

label "text_p1_name"
encode_string "Ranger"
label "text_p2_name"
encode_string "Magic-Us\ber"
label "text_p3_name"
encode_string "Barbarian"
label "text_p4_name"
encode_string "Fighter"

label "text_p1_controls"
encode_string "WSZXQC"
label "text_p2_controls"
encode_string "OKNMPB"
label "text_p3_controls"
encode_string "L. \bJ\bstk"
label "text_p4_controls"
encode_string "R. \bJ\bstk"

label "text_p1_controls_coco"
encode_string "2W3451"
label "text_p2_controls_coco"
encode_string "P+<>O?"

label "text_brackets"
encode_string "<  >"

label "text_skill"
encode_string "<  > Difficulty:"

label "text_easy"
encode_string "Easy"
label "text_hard"
encode_string "Hard"

label "text_vmode"
encode_string "<  > Video mode:"

label "text_pal"
encode_string "PAL"
label "text_ntsc0"
encode_string "NTSC 1"
label "text_ntsc1"
encode_string "NTSC 2"

label "text_to_play"
encode_string "<Space> to play"

label "text_space"
encode_string "Space"

label "textlist_select_screen"
ptr_print_at 25 21 "text_logo"
ptr_print_at 54 6 "text_brackets"
ptr_print_at 54 28 "text_p1_name"
label "PATCH_text_p1_controls_ptr"
ptr_print_at 54 77 "text_p1_controls"
ptr_print_at 68 6 "text_brackets"
ptr_print_at 68 28 "text_p2_name"
label "PATCH_text_p2_controls_ptr"
ptr_print_at 68 77 "text_p2_controls"
ptr_print_at 82 6 "text_brackets"
ptr_print_at 82 28 "text_p3_name"
ptr_print_at 82 77 "text_p3_controls"
ptr_print_at 96 6 "text_brackets"
ptr_print_at 96 28 "text_p4_name"
ptr_print_at 96 77 "text_p4_controls"
ptr_print_at 118 6 "text_skill"
ptr_print_at 132 6 "text_vmode"
ptr_print_at 160 29 "text_to_play"
textlist_done

label "charlist_select_screen"
print_char_at 54 9 "1"
print_char_at 68 9 "2"
print_char_at 82 9 "3"
print_char_at 96 9 "4"
print_char_at 118 9 "D"
print_char_at 132 9 "V"
textlist_done
