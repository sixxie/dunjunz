; Dunjunz - remade for the Dragon 32/64
; Copyright (c) 2017-2018 Ciaran Anscomb
;
; See COPYING file for redistribution conditions.

; simple easy-to-compress text mode initial loading screen

		rzb 3*32,$80
		rzb 32,$fc
		fci /  LOADING: DUNJUNZ              /
		rzb 32,$f3
		rzb 4*32,$80
		rzb 32,$fc
		fci /                  TEIPEN MWNCI  /
		rzb 32,$f3
		rzb 3*32,$80
