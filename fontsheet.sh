#!/bin/bash

PATH=/bin:/usr/bin
export PATH

. sheetfuncs.sh

test -x "$TILE2BIN" || { echo "$TILE2BIN: not executable" >&2; exit 1; }

sheet="fonts.png"

label () {
	echo "$1"
	echo
}

label "textfont"

# Ordered such that for characters requiring a descender (comma, g/y, p, and
# q), bits 1-2 indicate type of descender.  g and y share descender type 00.

font8x6w "$sheet"  56 24 2 -r	# g (desc 00)
font8x6w "$sheet"   0  0 0 -r	# space
font8x6w "$sheet" 112  0 0 -r	# full stop (comma)
font8x6w "$sheet"   8  0 0 -r	# exclamation
font8x6w "$sheet"   0 30 2 -r	# p (desc 10)
font8x6w "$sheet"  24  0 2 -r	# #
font8x6w "$sheet"   8 30 2 -r	# q (desc 11)
font8x6w "$sheet"  48  0 2 -r	# &

font8x6w "$sheet"  72 30 2 -r	# y (desc 00)
font8x6w "$sheet" 104  0 0 -r	# hyphen
font8x6w "$sheet"  72 24 0 -r	# i

font8x6w "$sheet"  88  0 2 -r	# +

font8x6w "$sheet"   0  6 2 -r	# 0
font8x6w "$sheet"   8  6 2 -r	# 1
font8x6w "$sheet"  16  6 2 -r	# 2
font8x6w "$sheet"  24  6 2 -r	# 3
font8x6w "$sheet"  32  6 2 -r	# 4
font8x6w "$sheet"  40  6 2 -r	# 5
font8x6w "$sheet"  56  6 2 -r	# 7
font8x6w "$sheet"  64  6 2 -r	# 8
font8x6w "$sheet"  72  6 2 -r	# 9

font8x6w "$sheet"  80  6 0 -r	# :
font8x6w "$sheet"  96  6 1 -r	# less-than
font8x6w "$sheet" 112  6 1 -r	# greater-than
font8x6w "$sheet" 120  6 2 -r	# question mark

font8x6w "$sheet"   8 12 3 -r	# A
font8x6w "$sheet"  16 12 2 -r	# B
font8x6w "$sheet"  24 12 2 -r	# C
font8x6w "$sheet"  32 12 2 -r	# D
font8x6w "$sheet"  40 12 2 -r	# E
font8x6w "$sheet"  48 12 2 -r	# F
font8x6w "$sheet"  56 12 2 -r	# G

font8x6w "$sheet"  64 12 2 -r	# H
font8x6w "$sheet"  72 12 2 -r	# I
font8x6w "$sheet"  80 12 3 -r	# J
font8x6w "$sheet"  88 12 2 -r	# K
font8x6w "$sheet"  96 12 2 -r	# L
font8x6w "$sheet" 104 12 3 -r	# M
font8x6w "$sheet" 112 12 2 -r	# N
font8x6w "$sheet" 120 12 2 -r	# O

font8x6w "$sheet"   0 18 2 -r	# P
font8x6w "$sheet"   8 18 3 -r	# Q
font8x6w "$sheet"  16 18 2 -r	# R
font8x6w "$sheet"  24 18 2 -r	# S
font8x6w "$sheet"  32 18 2 -r	# T
font8x6w "$sheet"  40 18 2 -r	# U
font8x6w "$sheet"  48 18 2 -r	# V
font8x6w "$sheet"  56 18 3 -r	# W

font8x6w "$sheet"  64 18 2 -r	# X
font8x6w "$sheet"  72 18 2 -r	# Y
font8x6w "$sheet"  80 18 2 -r	# Z
font8x6w "$sheet" 120 18 1 -r	# _

font8x6w "$sheet"   8 24 2 -r	# a
font8x6w "$sheet"  16 24 2 -r	# b
font8x6w "$sheet"  24 24 1 -r	# c
font8x6w "$sheet"  32 24 2 -r	# d
font8x6w "$sheet"  40 24 2 -r	# e
font8x6w "$sheet"  48 24 1 -r	# f

font8x6w "$sheet"  64 24 2 -r	# h
font8x6w "$sheet"  88 24 2 -r	# k
font8x6w "$sheet"  96 24 0 -r	# l
font8x6w "$sheet" 104 24 3 -r	# m
font8x6w "$sheet" 112 24 2 -r	# n
font8x6w "$sheet" 120 24 2 -r	# o

font8x6w "$sheet"  16 30 1 -r	# r
font8x6w "$sheet"  24 30 2 -r	# s
font8x6w "$sheet"  32 30 1 -r	# t
font8x6w "$sheet"  40 30 2 -r	# u
font8x6w "$sheet"  48 30 2 -r	# v
font8x6w "$sheet"  56 30 3 -r	# w
font8x6w "$sheet"  80 30 1 -r	# z

label "lnum_tiles_start"

for i in 0 1 2 3 4 5 6 7 8 9; do
	label "tile_lnum_$i"
	font8x7 "$sheet" $((i*8)) 36
done

label "lnum_tiles_end"

# mini-logo follows large numbers so they can be palette-swapped in one go

label "logo"
"$TILE2BIN" -x 0 -y 48 -w 120 -h 17 "$sheet" | "$BIN2S"
label "logo_end"

# small numbers are palette-swapped differently

label "snum_tiles_start"

for i in 1 2 3 4 5 6 7 8 9; do
	font8x5 "$sheet" $((i*8)) 43
done

label "snum_tiles_end"
