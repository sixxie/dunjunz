# Dunjunz - remade for the Dragon 32/64
# Copyright (c) 2017-2018 Ciaran Anscomb
#
# See COPYING file for redistribution conditions.

all: dunjunz.cas dunjunz.wav dunjunz.dsk
.PHONY: all

PACKAGE = dunjunz
VERSION = 1.1
distdir = $(PACKAGE)-$(VERSION)

####

ASM6809 = asm6809 -v
BIN2CAS = bin2cas.pl
B2CFLAGS = -r 44100
CFLAGS += -std=c99
CLEAN =
EXTRA_DIST =

####

./tile2bin: CFLAGS += $(shell sdl-config --cflags)
./tile2bin: LDLIBS += $(shell sdl-config --libs) -lSDL_image

CLEAN += ./tile2bin

####

%.bin: %.s
	$(ASM6809) $(AFLAGS) -l $(<:.s=.lis) -o $@ $<

%.dz: %
	dzip -c $< > $@

%.bin: %.txt
	perl -pe 'chomp if eof' $< | tr 'g ,\!p\-q:y.1234<>?ABCDEFGHLMNPRSTUVYabcdefhiklmnorstuvw\012' '\200\001\202\003\204\005\206\007\210\002\011-\064\177' > $@
	/bin/echo -ne '\00' >> $@

%.s: %.map ./map2s.pl
	echo "	include \"objects.s\"" > $@
	echo >> $@
	./map2s.pl $< >> $@

####

./fontsheet.sh: ./tile2bin
./textsheet.sh: ./tile2bin
./textsheet64.sh: ./tile2bin
./tilesheet.sh: ./tile2bin

tiles.s: tiles.png ./tilesheet.sh sheetfuncs.sh
tiles.s:
	./tilesheet.sh > $@

CLEAN += tiles.s

fonts.s: fonts.png ./fontsheet.sh sheetfuncs.sh
fonts.s:
	./fontsheet.sh > $@

CLEAN += fonts.s

TEXT_INCLUDES = \
	select-screen.txt \
	death-screen.txt \
	end-screen.txt \
	credits-screen.txt \
	credits-screen2.txt
text.s: $(TEXT_INCLUDES) ./textsheet.sh sheetfuncs.sh
text.s:
	./textsheet.sh > $@

CLEAN += text.s

text64.s: ./textsheet64.sh sheetfuncs.sh
text64.s:
	./textsheet64.sh > $@

CLEAN += text64.s

####

LEVELS = 01 02 03 04 05 06 \
	07 08 09 10 11 12 \
	13 14 15 16 17 18 \
	19 20 21 22 23 24 25 25d

LEVELS_S = $(LEVELS:%=level%.s)
LEVELS_BIN = $(LEVELS:%=level%.bin)

CLEAN += $(LEVELS_S) $(LEVELS_BIN) $(LEVELS:%=level%.lis)

level%.bin: level%.s objects.s
	$(ASM6809) -B -l $(<:.s=.lis) -o $@ $<

####

$(LEVELS_S): objects.s

play-screen.bin: play-screen.png ./tile2bin
	./tile2bin -o $@ $<

text-screen.bin: text-screen.png ./tile2bin
	./tile2bin -o $@ $<

CLEAN += play-screen.bin play-screen.bin.dz
CLEAN += text-screen.bin text-screen.bin.dz

notefreq.s: ./gen_notefreq.pl
	./gen_notefreq.pl -c 58 > $@

CLEAN += notefreq.s

dunj64.bin: song.s text64.s
dunj64.bin: dunj64.s
	$(ASM6809) -B -l dunj64.lis -s dunj64.sym -E dunj64.exp -o $@ $<
CLEAN += dunj64.lis dunj64.sym dunj64.exp dunj64.bin dunj64.bin.dz

dunjunz.bin: dunj64.bin notefreq.s tiles.s fonts.s text.s
dunjunz.bin: play-screen.bin.dz text-screen.bin.dz
dunjunz.bin: $(LEVELS_BIN)
dunjunz.bin: dunjunz.s
	$(ASM6809) -B -l dunjunz.lis -s dunjunz.sym -E dunjunz.exp -o $@ $<
CLEAN += dunjunz.lis dunjunz.sym dunjunz.exp dunjunz.bin dunjunz.bin.dz

title.bin: title.png ./tile2bin
	./tile2bin -o $@ $<

CLEAN += title.bin title.bin.dz

copyright.bin: copyright.png ./tile2bin
	./tile2bin -o $@ $<

CLEAN += copyright.bin copyright.bin.dz

copyright64.bin: copyright64.png ./tile2bin
	./tile2bin -o $@ $<

CLEAN += copyright64.bin copyright64.bin.dz

####

# Tape image

TAPE_PARTS = tape-stage2 \
	tape-part1 \
	tape-part2 \
	tape-part3

TAPE_PARTS_CAS = $(TAPE_PARTS:%=%.cas)
TAPE_PARTS_WAV = $(TAPE_PARTS:%=%.wav)

# Second stage loader for tape (bin2cas.pl generates the first stage).  dzip
# doesn't save much for this (3 bytes at time of writing!), but it's required
# so that dunzip code used later on is included in the first stage by
# bin2cas.pl.

tape-stage2.bin: title0.s dunjunz.bin.dz tape-part1.bin
tape-stage2.bin: tape-stage2.s
	$(ASM6809) -C -e start -l tape-stage2.lis -o $@ $<

CLEAN += tape-stage2.bin tape-stage2.lis

# Part 1 contains the loading screen (separately, so it can be reused as the
# title page on 64K machines) and copyright messages to be overlaid onto it.

tape-part1.bin: title.bin.dz copyright.bin.dz copyright64.bin.dz
tape-part1.bin: tape-part1.s
	$(ASM6809) -B -l tape-part1.lis -s tape-part1.sym -E tape-part1.exp -o $@ $<

CLEAN += tape-part1.bin tape-part1.lis tape-part1.sym tape-part1.exp

# Part 2 is the main game.

# Part 3 is only loaded on 64K machines, and contains the code to display the
# title graphics (contained in part 1), music data, and playback routine.

# Rules to create the tape images.

dunjunz.cas dunjunz.wav: tape-stage2.bin tape-part1.bin dunjunz.bin.dz dunj64.bin.dz
	$(BIN2CAS) $(B2CFLAGS) --cue --autorun --eof-data --fast -o $@ -n DUNJUNZ \
		-C --dzip --zload 0x2600 tape-stage2.bin \
		--vdg 0x08 \
		--omit --no-dzip \
		-B tape-part1.bin \
		-B dunjunz.bin.dz \
		-B dunj64.bin.dz

CLEAN += dunjunz.cas dunjunz.wav

####

# Disk image

DISK_PARTS = disk-boot.bin \
	disk-part1-dragon.bas \
	disk-part2-dragon.bin \
	disk-part3-dragon.bin \
	disk-part1-coco.bas \
	disk-part2-coco.bin \
	disk-part3-coco.bin

# Boot block.  Same image is used for DragonDOS and RSDOS disks.

disk-boot.bin: disk-boot.s title0.bin.dz
	$(ASM6809) -B -l disk-boot.lis -o $@ $<

CLEAN += disk-boot.bin disk-boot.lis
CLEAN += title0.lis title0.bin title0.bin.dz

# Part 1 is a BASIC loader that is simply transferred to the disk image.

disk-part1-dragon.bas: disk-part2-dragon.bin disk-part3-dragon.bin

disk-part1-dragon.bas: disk-part1-dragon.in
	./sym-subst.pl disk-part2-dragon.sym disk-part3-dragon.sym < $< > $@

# Part 2 contains the title screen, copyright messages and the extra data for
# 64K machines.  The extras will be thrown away on 32K machines, but that's ok:
# disks are fast!

disk-part2-dragon.bin: title.bin.dz copyright.bin.dz
disk-part2-dragon.bin: copyright64.bin.dz dunj64.bin.dz
disk-part2-dragon.bin: disk-part2.s
	$(ASM6809) -D -e DUNJ2_exec -s disk-part2-dragon.sym -l disk-part2-dragon.lis -o $@ $<

CLEAN += disk-part2-dragon.bin disk-part2-dragon.lis disk-part2-dragon.sym

disk-part2-coco.bin: title.bin.dz copyright.bin.dz
disk-part2-coco.bin: copyright64.bin.dz dunj64.bin.dz
disk-part2-coco.bin: disk-part2.s
	$(ASM6809) -C -e DUNJ2_exec -l disk-part2-coco.lis -o $@ $<

CLEAN += disk-part2-coco.bin disk-part2-coco.lis

# Part 3 contains the main game.

disk-part3-dragon.bin: dunjunz.bin.dz
disk-part3-dragon.bin: disk-part3.s
	$(ASM6809) -D -e DUNJ3_exec -s disk-part3-dragon.sym -l disk-part3-dragon.lis -o $@ $<

CLEAN += disk-part3-dragon.bin disk-part3-dragon.lis disk-part3-dragon.sym

disk-part3-coco.bin: dunjunz.bin.dz
disk-part3-coco.bin: disk-part3.s
	$(ASM6809) -C -e DUNJ3_exec -l disk-part3-coco.lis -o $@ $<

CLEAN += disk-part3-coco.bin disk-part3-coco.lis

# Rules to create the disk image.

# Sadly, I don't have tools to manipulate DragonDOS filesystems yet, so they
# need to be copied on manually once the hybrid image is created.

dunjunz.dsk: $(DISK_PARTS)
	rm -f $@
	cp hybrid.dsk $@
	dd if=disk-boot.bin of=dunjunz.dsk bs=256 seek=2 conv=notrunc
	dd if=disk-boot.bin of=dunjunz.dsk bs=256 seek=$(shell expr 34 \* 18) conv=notrunc
	decb copy -0bt disk-part1-coco.bas dunjunz.dsk,DUNJUNZ.BAS
	decb copy -2b disk-part2-coco.bin $@,DUNJ2.BIN
	decb copy -2b disk-part3-coco.bin $@,DUNJ3.BIN
	@echo Copy Dragon files on manually for now

CLEAN += dunjunz.dsk dunjunz.dsk.bak

####

dist: $(EXTRA_DIST)
	git archive --format=tar --output=./$(distdir).tar --prefix=$(distdir)/ HEAD
	tar -r -f ./$(distdir).tar --owner=root --group=root --mtime=./$(distdir).tar --transform "s,^,$(distdir)/," $(EXTRA_DIST)
	gzip -f9 ./$(distdir).tar
.PHONY: dist

CLEAN += $(distdir).tar $(distdir).tar.gz

####

clean:
	rm -f $(CLEAN)
.PHONY: clean
