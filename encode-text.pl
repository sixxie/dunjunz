#!/usr/bin/perl -wT
use strict;

# Map characters to font indices with descended information.
#
# Character order is chosen such that bits 1-2 represent which style of
# descender is required.
#
# This could be done with an invocation of 'tr', but the arguments become
# pretty complex.

my $i = 0;
my %charmap = ();
my %have_base = ();

# Bunch all specials (using descenders) at the beginning.  Include any
# non-specials that interleave with them here just for clarity.
add_special(
	"\000", 0000,
	'g',    0200,
	' ',    0001,
	'.',    0002,
	',',    0202,
	'!',    0003,
	'p',    0204,
	'#',    0005,
	'q',    0206,
	'&',    0007,
	'y',    0210,
	'-',    0011,
	'i',    0012,
	'j',    0212,
	'~',    0175,
	"\010", 0176,
	"\012", 0177);

add_simple(               '+',                   );
add_simple('0', '1', '2', '3', '4', '5',      '7');
add_simple('8', '9', ':',      '<',      '>', '?');
add_simple(     'A', 'B', 'C', 'D', 'E', 'F', 'G');
add_simple('H', 'I', 'J', 'K', 'L', 'M', 'N', 'O');
add_simple('P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W');
add_simple('X', 'Y', 'Z',                        );
add_simple('_', 'a', 'b', 'c', 'd', 'e', 'f',    );
add_simple('h',           'k', 'l', 'm', 'n', 'o');
add_simple(          'r', 's', 't', 'u', 'v', 'w');
add_simple(          'z',                        );

while (read(STDIN, my $c, 1) == 1) {
	if (exists $charmap{$c}) {
		print pack("C", $charmap{$c});
	} else {
		print pack("C", 1);
	}
}

sub add_special {
	while (@_) {
		my $c = shift;
		my $v = shift;
		$charmap{$c} = $v;
		$have_base{$v} = 1;
		$have_base{$v & 0177} = 1;
	}
}

sub add_simple {
	for my $c (@_) {
		next unless defined $c;
		$i++ while (exists $have_base{$i});
		$have_base{$i} = 1;
		$charmap{$c} = $i;
	}
}
