; Dunjunz - remade for the Dragon 32/64
; Copyright (c) 2017-2018 Ciaran Anscomb
;
; See COPYING file for redistribution conditions.

; ========================================================================

; There are 4 x 6 rooms in each map.  Each room has 8 x 8 positions.  Room
; IDs are organised like this:

; -$80  -$50  -$20  $10
; -$78  -$48  -$18  $18
; -$70  -$40  -$10  $20
; -$68  -$38  -$08  $28
; -$60  -$30   $00  $30
; -$58  -$28   $08  $38

; Object positions (y,x) are not strictly cartesian.  The 'y' coordinate
; is a room ID plus vertical offset within the room.  The 'x' coordinate
; is just horizontal offset into the room.  Thus, 'y' ranges from -$80 to
; $3f, and 'x' ranges from 0 to 7.
;
; Two simple bitmaps are maintained.  'bmp_wall' tracks where walls and
; doors are (anything that blocks player movement).  Doors are cleared
; within the bitmap when opened.  'bmp_objects' indicates where objects
; are, consulted both for pickups and undrawing.
;
; In each bitmap, one byte represents all positions for one 'y'
; coordinate.  Most significant bit maps to leftmost position ('x' == 0).

; ========================================================================

; Definitions

DEBUG_LEVEL25	equ 0

		include	"dragonhw.s"
		include "objects.s"
		include "notefreq.s"
		include "dunj64.exp"

fb0		equ $0200	; framebuffer base
fb_w		equ 32		; framebuffer line width
fb_h		equ 192		; framebuffer height
fb0_end		equ fb0+fb_w*fb_h

; Tile dimensions in pixels.  Information only.  Check 'yx_to_woff' for
; how (y,x) is actually mapped to window offsets.

tile_w		equ 12
tile_h		equ 9

; Size of wall and pickup bitmaps.

bmp_w		equ 4
bmp_h		equ 48
sizeof_bmp	equ bmp_w*bmp_h

; Window base addresses

p1_win		equ fb0+20*fb_w+1
p2_win		equ fb0+20*fb_w+14
p3_win		equ fb0+100*fb_w+1
p4_win		equ fb0+100*fb_w+14

; Stats window base addresses

p1_swin		equ fb0+21*fb_w+27
p2_swin		equ p1_swin+40*fb_w
p3_swin		equ p1_swin+80*fb_w
p4_swin		equ p1_swin+120*fb_w

; Player window dimensions in tiles.  Here for information only: code
; assumes these values and they must not change.

win_w		equ 8
win_h		equ 8

; Maximum numbers of certain types of object.

max_doors	equ 20
max_keys	equ 20
max_items	equ 30		; including exit
max_trapdoors	equ 8		; *must* be this amount of trapdoors

max_monsters	equ 28		; needn't all be active at once

; Players, monsters, shots: need to know which way each of them is facing.
; Multiples of 2 for indexing into jump tables.  Order important as bit
; twiddling is used to reverse the direction of shots.

facing_up	equ 0
facing_down	equ 2
facing_left	equ 4
facing_right	equ 6

; ========================================================================

; Structures
; ----------

; _obj_ - object.  Type is indicated by 'tile'.  Reserves a few bytes of
; arbitrary data, use of which is type-dependent.

		org 0
obj_y		rmb 1		; 'y' coordinate in map (-$80 to $3f)
obj_x		rmb 1		; 'x' coordinate in map (0 to 7)
obj_tile	rmb 1		; tile id
obj_woff	rmb 2		; precalc offset into window
obj_data	rmb 3
sizeof_obj

obj_drainer_hp	equ obj_data
obj_key_opens	equ obj_data
obj_shot_facing	equ obj_data
obj_shot_plr	equ obj_data+1

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; _spr_ - sprite.  Extends _obj_.

spr_org_	org 0-spr_offset_

spr_next_y	rmb 1		; destination 'y' when moving
spr_next_x	rmb 1		; destination 'x' when moving

spr_hp		rmb 1		; hit points (BCD)
spr_state	rmb 1		; state machine index

spr_cx		rmb 1		; current 'x' - see note

spr_offset_	equ *-spr_org_
		rmb sizeof_obj

spr_tilebase	rmb 1		; lowest tile id for player tiles

sizeof_sprite	equ *+spr_offset_

spr_facing	equ obj_data	; one of facing_*
spr_owoff	equ obj_data+1	; origin win offset (undraw during move)
spr_tmp0	equ obj_data+1	; use as a counter when not moving

; Note: spr_cx is only used for its lowest bit, which is modified during
; horizontal movement and changes which tileset to use when drawing.

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; _mon_ - monster.  Inherits from _spr_.

mon_org_	org 0-mon_offset_

mon_offset_	equ *-mon_org_+spr_offset_
		rmb sizeof_sprite

sizeof_mon	equ *+mon_offset_

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; _plr_ - player.  Extends _spr_.

plr_org_	org 0-plr_offset_

; 'control' - populated while reading keyboard/joysticks.
;   $00 = no action
;   $01 = fire
;   $02 = magic
;   $80|facing = move

plr_control	rmb 1

plr_nshots	rmb 1		; number of active shots (<= ammo)
plr_key_opens	rmb 2		; (y,x) of door that held key opens
plr_speed	rmb 1		; non-zero = player moves fast!
plr_score2	rmb 1		; BCD
plr_score0	rmb 1		; BCD
plr_yroom	rmb 1		; obj_y & 0xf8
plr_bit		rmb 1		; bit within playing/done

plr_offset_	equ *-plr_org_+spr_offset_
		rmb sizeof_sprite

plr_swin	rmb 2		; address of stats window
plr_win		rmb 2		; address of player window
plr_armour	rmb 1		; armour
plr_power	rmb 1		; weapon power
plr_ammo	rmb 1		; ammunition (1 to 3)

sizeof_plr	equ *+plr_offset_

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; _lvl_ - level source data.

		org 0
lvl_doors	rmb max_doors*2		; y,x
lvl_keys	rmb max_keys*2		; y,x
lvl_items	rmb max_items*3		; tile,y,x
lvl_trapdoors	rmb max_trapdoors*2	; y,x
lvl_bmp_wall	rmb sizeof_bmp		; wall bitmap
sizeof_lvl

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Keyboard table entry macros to reduce possibility of error.

KEYDSP_ENTRY	macro
		fcb ~(1<<\1)		; column
		fcb 1<<\2		; row
		fdb \3			; dispatch
		endm

KEYTBL_ENTRY	macro
		fcb ~(1<<\1)		; column
		fcb 1<<\2		; row
		fcb \3			; action
		endm

; Head up a state machine jump table with this macro.

STATE_JTBL	macro
19824		equ *
		endm

; Use this macro to create symbols for specific states.

STATE_ID	macro
\1		equ *-19824B
		endm

; Patches that directly modify code and data to cope with different
; machine or display variants use a common format:

;   First entry:
;     patch byte count (0-255, 0 == 256 bytes)
;     base address
;     bytes...
;   Subsequent entries:
;     patch byte count (1-127, 0 == done, top bit set == 16-bit skip)
;     skip count
;       8-bit or 16-bit signed offset
;     bytes...

; Macro use:
;
; First use of PATCH macro (or after ENDPATCHSET) is special
; and embeds the absolute address of the first area to patch.

;               PATCH first_patch_address
;               fcb data,data,data
;               ENDPATCH
;
; Subsequent uses of PATCH macro embed relative addresses, using a single
; byte if possible to save space.

;               PATCH second_patch_address
;               fcb data,data,data
;               ENDPATCH

; ENDPATCHSET macro finishes a patchset, flagged with a zero size byte.

;               ENDPATCHSET

_PATCH_ADDR	set -1		; initially flag no patch in progress

PATCH		macro
	if (_PATCH_ADDR == -1)
		fcb 19826F-19825F
		fdb \1
	elsif (\1-_PATCH_ADDR) >= -128 && (\1-_PATCH_ADDR) < 128
		fcb 19826F-19825F
		fcb \1-_PATCH_ADDR
	else
		fcb $80|(19826F-19825F)
		fdb (\1-_PATCH_ADDR)
	endif
_PATCH_ADDR	set \1+19826F-19825F
19825
		endm

ENDPATCH	macro
19826
		endm

ENDPATCHSET	macro
		fcb 0
_PATCH_ADDR	set -1
		endm

; ========================================================================

; Direct page variables

; Trying to keep player sprites here for now, as there are plenty of
; places where we manipulate them directly rather than as offsets into the
; player struct.

		org $0112
gamedp		equ *>>8

tmp0		rmb 2
dzip_end
tmp1		rmb 2
tmp2		rmb 2

playing		rmb 1		; bits 0-3 = player 1-4 active
finished	rmb 1		; bits 0-3 = player 1-4 finished level
dying		rmb 1		; bits 0-3 = player 1-4 dying or dead
dead		rmb 1		; bits 0-3 = player 1-4 dead
exiting		rmb 1		; non-zero = a player is using exit

; 'death_fifo' is a list of pointers to _plr_ struct in order of player
; death.  'death_fifo_end' points to end of list, where next dead player
; is stored.  FIFO is shifted on resurrection.

death_fifo_end	rmb 2		; ptr to end of death fifo
death_fifo	rmb 8

finish_delay	rmb 1

video_mode	rmb 1		; 0=pal, 1=ntsc phase0, 2=ntsc phase1

; Font rendering
text_descender	rmb 1
text_char_width	rmb 1
text_line_x	rmb 1
text_cursor_y	rmb 1
text_cursor_x	rmb 1
text_colour	rmb 1
text_tmp0	rmb 2
text_tmp1	rmb 2

colour_bg	rmb 1
colour_fg	rmb 1
colour_hi	rmb 1
colour_lo	rmb 1

; PSG channel priority
psg_c1pri	rmb 1
psg_c2pri	rmb 1

difficulty	rmb 1
level		rmb 1

anim_count	rmb 1

p3_moved	rmb 1
p4_moved	rmb 1

; Player structs

players		equ *+plr_offset_
player1		equ *+plr_offset_
		rmb sizeof_plr
player2		equ *+plr_offset_
		rmb sizeof_plr
player3		equ *+plr_offset_
		rmb sizeof_plr
player4		equ *+plr_offset_
		rmb sizeof_plr
players_end	equ *+plr_offset_

; ========================================================================

		org fb0_end

; Explicitly order the sections by listing them here.

CODE_start_	CODE
DATA_start_	DATA
BSS_start_	BSS
		CODE

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Programmable Sound Generator & Mixer.

; Either channel can be configured as a sawtooth or square wave using
; self-modifying code.  The core for each channel always generates a
; sawtooth wave.  The result is ANDed, either with $FF (no change - still
; sawtooth), or $80 (square wave).  This is then shifted so that summing
; can't overflow.
;
; The square wave ends up having half the amplitude of the sawtooth, but
; in terms of RMS it's not too far off.
;
; Then envelope is applied, also using self-modifying code.  Two bytes are
; executed, generally either NOP or LSRA.  Two LSRA instructions would
; drop the amplitude of the channel to a quarter.

psg_play2frags
		bsr psg_playfrag
		; fall through to psg_playfrag

psg_playfrag
		lda #*>>8
		tfr a,dp
		setdp *>>8

		ldy #psg_saw_att

		; channel 1 "envelope"
psg_c1env	equ *+1
		ldu #psg_f1		; convenient initial zero
		lda ,u+			; attenuation
		bne 10F			; end of list
		; end of list
		sta psg_c1pri
		sta psg_a1_sqr	; even for sawtooth
		bra 90F
10		pulu x			; frequency
		stx psg_f1
		tsta
		bpl 20F
		; -ve attenuation: fetch from sawtooth attenuation table
		ldd a,y
		std psg_a1_saw
		bra 30F
20		sta psg_a1_sqr
30		stu psg_c1env
90

		; channel 2 "envelope"
psg_c2env	equ *+1
		ldu #psg_f2		; convenient initial zero
		lda ,u+			; attenuation
		bne 10F			; end of list
		; end of list
		sta psg_c2pri
		sta psg_a2_sqr	; even for sawtooth
		bra 90F
10		pulu x			; frequency
		stx psg_f2
		tsta
		bpl 20F
		; -ve attenuation: fetch from sawtooth attenuation table
		ldd a,y
		std psg_a2_saw
		bra 30F
20		sta psg_a2_sqr
30		stu psg_c2env
90

		lda psg_c1pri
		ora psg_c2pri
		beq 10F
		ldb #$3d
		stb reg_pia1_crb	; enable sound mux
10

		ldx #101		; fragment length
		ldu #reg_pia1_pdra	; DAC

		; - - - - - - - - - - - - - - - - - - - - - - -

psg_core_loop

psg_o1		equ *+1
                ldd #$0000		; 3
psg_f1		equ *+1
                addd #$0000		; 4
		std psg_o1		; 5
psg_mod1	lsla			; 2 - set to lsra for sawtooth
psg_a1_saw	rorb			; 2 - or sawtooth shift/nop
		sex			; 2 - or sawtooth shift/nop
psg_a1_sqr	equ *+1
		anda #$00		; 2 - always $ff for sawtooth
		sta psg_c1val		; 4

psg_o2		equ *+1
                ldd #$0000		; 3
psg_f2		equ *+1
                addd #$0000		; 4
		std psg_o2		; 5
psg_mod2	lsla			; 2 - set to lsra for sawtooth
psg_a2_saw	rorb			; 2 - or sawtooth shift/nop
		sex			; 2 - or sawtooth shift/nop
psg_a2_sqr	equ *+1
		anda #$00		; 2 - always $ff for sawtooth

psg_c1val	equ *+1
		adda #$00		; 2

		sta ,u			; 4
		leax -1,x		; 5
		bne psg_core_loop	; 3
					; total 58 cycles

		; - - - - - - - - - - - - - - - - - - - - - - -

		ldd #$3580
		sta 3,u			; disable sound mux
		stb ,u			; centre DAC

		lda #gamedp
		tfr a,dp
		setdp gamedp
		rts

; Set up a sequence to be played.  Find lower priority channel and
; populate its data.

; Sound data is a list of (attenuation,frequency).  Zero ends the list.

; Entry:
; 	A = priority
; 	X -> sound data

psg_submit
		ldb psg_c1pri
		cmpb psg_c2pri
		bhi psg_submit_c2

psg_submit_c1	cmpa psg_c1pri
		blo 99F
		sta psg_c1pri
		stx psg_c1env
		ldd #$0000
		std psg_o1
		lda ,x		; test first attenuation value
		bmi 10F		; negative = sawtooth
		; square wave
		lda #$48	; lsla
		sta psg_mod1
		ldd #$561d	; rorb,sex
		std psg_a1_saw
99		rts
		; sawtooth wave
10		ldd #$44ff	; lsra, (anda) #$ff
		sta psg_mod1
		stb psg_a1_sqr
		rts

psg_submit_c2	cmpa psg_c2pri
		blo 99F
		sta psg_c2pri
		stx psg_c2env
		ldd #$0000
		std psg_o2
		lda ,x		; test first attenuation value
		bmi 10F		; negative = sawtooth
		; square wave
		lda #$48	; lsla
		sta psg_mod2
		ldd #$561d	; rorb,sex
		std psg_a2_saw
99		rts
		; sawtooth wave
10		ldd #$44ff	; lsra, (anda) #$ff
		sta psg_mod2
		stb psg_a2_sqr
		rts

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Sound data

psg_asqr_0	equ 127
psg_asqr_2	equ 101
psg_asqr_4	equ 80
psg_asqr_6	equ 64
psg_asqr_8	equ 51
psg_asqr_10	equ 40
psg_asqr_12	equ 32
psg_asqr_14	equ 25
psg_asqr_16	equ 20
psg_asqr_18	equ 16
psg_asqr_20	equ 13
psg_asqr_22	equ 10
psg_asqr_24	equ 8
psg_asqr_26	equ 6
psg_asqr_28	equ 5
psg_asqr_off	equ 0

		DATA
		fcb $4f		; clra
		fcb $44		; lsra
		fcb $44		; lsra
		fcb $12		; nop
		fcb $12		; nop
psg_saw_att
		CODE

; These index into psg_saw_att
psg_asaw_0	equ -2		; nop,nop
psg_asaw_6	equ -3		; lsra,nop
psg_asaw_12	equ -4		; lsra,lsra
psg_asaw_off	equ -5		; clra,lsra

; Single note
NOTE		macro		; note,volume
		fcb \2
		fdb \1
		endm

; Two notes at same volume
NOTE2		macro		; note,note,volume
		fcb \3
		fdb \1
		fcb \3
		fdb \2
		endm

; Double length note
DNOTE		macro		; note,volume
		fcb \2
		fdb \1
		fcb \2
		fdb \1
		endm

; Double length note, volume changes
ENOTE		macro		; note,volume,volume
		fcb \2
		fdb \1
		fcb \3
		fdb \1
		endm

		DATA
snd_key
		ENOTE G6,psg_asqr_0,psg_asqr_4
		fcb 0

snd_hit
		NOTE C4,psg_asqr_0
		NOTE A3,psg_asqr_0
		fcb 0

snd_bump
		NOTE F3,psg_asqr_0
		fcb 0

snd_monster_hit
		NOTE E4,psg_asaw_12
		NOTE C4,psg_asaw_0
		fcb 0

snd_monster_die
		ENOTE C4,psg_asaw_0,psg_asaw_6
		ENOTE E4,psg_asaw_0,psg_asaw_6
		ENOTE C4,psg_asaw_0,psg_asaw_6
		ENOTE E4,psg_asaw_6,psg_asaw_12
		ENOTE E4,psg_asaw_6,psg_asaw_12
		fcb 0

snd_drainer
		NOTE2 G2,G1,psg_asqr_4
		NOTE2 G2,G1,psg_asqr_4
		NOTE2 G2,G1,psg_asqr_4
		NOTE2 G2,G1,psg_asqr_4
		fcb 0

snd_low
		ENOTE C4,psg_asaw_12,psg_asaw_6
		ENOTE C4+20,psg_asaw_6,psg_asaw_6
		ENOTE C4,psg_asaw_0,psg_asaw_0
		ENOTE C4+20,psg_asaw_0,psg_asaw_0
		fcb 0

snd_money
		ENOTE C7,psg_asqr_4,psg_asqr_0
		ENOTE C7-20,psg_asqr_0,psg_asqr_0
		ENOTE C7,psg_asqr_0,psg_asqr_4
		ENOTE C7-20,psg_asqr_8,psg_asqr_8
		fcb 0

snd_fire
		ENOTE C5,psg_asaw_12,psg_asaw_6
		fcb 0

snd_door
		DNOTE F5,psg_asqr_0
		DNOTE F4,psg_asqr_4
		DNOTE F4,psg_asqr_8
		fcb 0

snd_food
		ENOTE G5,psg_asaw_0,psg_asaw_6
		ENOTE G5+20,psg_asaw_0,psg_asaw_6
		ENOTE G4,psg_asaw_0,psg_asaw_6
		ENOTE G4+20,psg_asaw_0,psg_asaw_6
		fcb 0

snd_magic
		ENOTE C4,psg_asaw_0,psg_asaw_12
		ENOTE E4,psg_asaw_0,psg_asaw_12
		ENOTE C5,psg_asaw_0,psg_asaw_12
		ENOTE C5,psg_asaw_0,psg_asaw_12
		ENOTE C4,psg_asaw_0,psg_asaw_12
		ENOTE E4,psg_asaw_0,psg_asaw_12
		ENOTE C5,psg_asaw_6,psg_asaw_12
		ENOTE C5,psg_asaw_6,psg_asaw_12
		ENOTE C4,psg_asaw_6,psg_asaw_12
		ENOTE E4,psg_asaw_6,psg_asaw_12
		ENOTE C5,psg_asaw_6,psg_asaw_12
		ENOTE C5,psg_asaw_6,psg_asaw_12
		ENOTE C4,psg_asaw_12,psg_asaw_12
		ENOTE E4,psg_asaw_12,psg_asaw_12
		ENOTE C5,psg_asaw_12,psg_asaw_12
		ENOTE C5,psg_asaw_12,psg_asaw_12
		fcb 0

snd_exit
		DNOTE C7,psg_asaw_0
		DNOTE (C7+B6)/2,psg_asaw_0
		DNOTE B6,psg_asaw_0
		DNOTE (B6+AS6)/2,psg_asaw_0
		DNOTE AS6,psg_asaw_0
		DNOTE (AS6+A6)/2,psg_asaw_0
		DNOTE A6,psg_asaw_0
		DNOTE (A6+GS6)/2,psg_asaw_0
		DNOTE GS6,psg_asaw_0
		DNOTE (GS6+G6)/2,psg_asaw_0
		DNOTE G6,psg_asaw_0
		DNOTE (G6+FS6)/2,psg_asaw_0
		DNOTE FS6,psg_asaw_0
		DNOTE (FS6+F6)/2,psg_asaw_0
		DNOTE F6,psg_asaw_0
		DNOTE (F6+E6)/2,psg_asaw_0
		DNOTE E6,psg_asaw_0
		DNOTE (E6+DS6)/2,psg_asaw_0
		DNOTE DS6,psg_asaw_0
		DNOTE (DS6+D6)/2,psg_asaw_0
		DNOTE D6,psg_asaw_6
		DNOTE (D6+CS6)/2,psg_asaw_6
		DNOTE CS6,psg_asaw_12
		DNOTE (CS6+C6)/2,psg_asaw_12
		fcb 0

snd_tport
		NOTE2 CS5,DS5,psg_asaw_12
		NOTE2 F5,G5,psg_asaw_12
		NOTE2 A5,B5,psg_asaw_12
		NOTE C6,psg_asaw_12
		NOTE CS5,psg_asaw_6
		NOTE2 DS5,F5,psg_asaw_6
		NOTE2 G5,A5,psg_asaw_6
		NOTE2 B5,C6,psg_asaw_6
		NOTE2 CS5,DS5,psg_asaw_0
		NOTE2 F5,G5,psg_asaw_0
		NOTE2 A5,B5,psg_asaw_0
		NOTE C6,psg_asaw_0
		NOTE2 CS5,DS5,psg_asaw_0
		NOTE2 F5,G5,psg_asaw_0
		NOTE2 A5,B5,psg_asaw_0
		NOTE C6,psg_asaw_0
		NOTE2 CS5,DS5,psg_asaw_0
		NOTE2 F5,G5,psg_asaw_0
		NOTE2 A5,B5,psg_asaw_0
		NOTE C6,psg_asaw_0
		NOTE2 CS5,DS5,psg_asaw_6
		NOTE2 F5,G5,psg_asaw_6
		NOTE2 A5,B5,psg_asaw_6
		NOTE C6,psg_asaw_6
		NOTE CS5,psg_asaw_12
		NOTE2 DS5,F5,psg_asaw_12
		NOTE2 G5,A5,psg_asaw_12
		NOTE2 B5,C6,psg_asaw_12
		fcb 0

		CODE

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Main game loop

		; tile set 0
anim_tiles_0	ldx #tile_drainer0_a
		stx mod_drainer_a
		ldx #tile_drainer0_b
		stx mod_drainer_b
		ldx #tile_money0_a
		stx mod_money_a
		ldx #tile_money0_b
		stx mod_money_b
		ldx #tile_power0_a
		ldu #tile_power0_b
		ldy #tile_chalice_nw0_a
		bra 10F

		; tile set 1
anim_tiles_1	ldx #tile_drainer1_a
		stx mod_drainer_a
		ldx #tile_drainer1_b
		stx mod_drainer_b
		ldx #tile_money1_a
		stx mod_money_a
		ldx #tile_money1_b
		stx mod_money_b
		ldx #tile_power1_a
		ldu #tile_power1_b
		ldy #tile_chalice_nw1_a
10		stx mod_power_a
		stu mod_power_b
		sty mod_chalice
mod_draw_chalice_0
                jsr update_chalice
		bra anim_done

; Two-frame animations toggle every 8 frames.  This is called from
; 'mainloop' below, and also during the end animation so that any drainers
; still visible continue to animate.

animate		inc anim_count
		lda anim_count
		anda #15
		beq anim_tiles_1
		anda #7
		beq anim_tiles_0
		; Redraw select range of object types every call.
anim_done	ldu #pickups
10		lda obj_tile,u
		cmpa #always_draw_tile
		blo 20F
		jsr obj_draw
20		leau sizeof_obj,u
		cmpu #all_objects_end
		blo 10B
		rts

mainloop

		bsr animate

		jsr process_monsters
		jsr plr_scan_controls
		jsr process_players
		jsr process_shots
		jsr psg_play2frags

		ldd #$fb40
		sta reg_pia0_pdrb
		bitb reg_pia0_pdra	; BREAK?
		bne 10F

		lda #$fe
		sta reg_pia0_pdrb
		bitb reg_pia0_pdra	; ENTER?
		beq 90F
		bra pause

10

		lda finished
		ora dead
		cmpa #$0f
		bne mainloop

		dec finish_delay
		bne mainloop

		lda dead
		cmpa playing
		bne finished_level

		lda #$ff
		sta level	; no cheating once all dead!
		ldy #death_screen	; dzip start
game_over_screen
		ldb colour_fg
		stb text_colour
		jsr unpack_text_screen
		jsr write_screen

		; a few seconds pause for reflection
		ldx #$2000
20		deca
		bne 20B
		leax -1,x
		bne 20B
90		jmp game_reset

pause
PATCH_pause	equ *+1
		jsr wait_keypress
		bra 10B

finished_level
		lda level
		cmpa #24
		beq end_animation
		jmp level_setup

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Ending animation.  A bit naff to be honest, just spins the dying monster
; animation around the chalice for a bit (like the original) then just
; ends.  Would be nice to squeeze in something a bit more impressive.

eanim_add
		lda obj_y,y
		cmpa #$80
		bne 99F
		pulu a,b
		std obj_y,y
		stb spr_cx,y
		jsr yx_to_woff
		stx obj_woff,y
		stx spr_owoff,y
		clr spr_state,y
		leay sizeof_mon,y
99		rts

mon_step4	bsr mon_step1
		bsr mon_step1
		bsr mon_step1
mon_step1	pshs u
		jsr mon_run_state
		puls u,pc

eanim_step	pshs u
		ldu #monsters
10		lda obj_y,u
		deca
		bvs 20F
		lda spr_state,u
		ldx #jtbl_eanim_state
		jsr [a,x]
20		leau sizeof_mon,u
		cmpu #monsters_end
		blo 10B
		puls u,pc

end_animation
		; first, destroy all monsters
		ldu #monsters
		ldb #state_mon_die
10		lda obj_y,u
		deca
		bvs 20F
		stb spr_state,u
20		leau sizeof_mon,u
		cmpu #monsters_end
		blo 10B
		; step monsters through to death
		bsr mon_step4

		ldd #1200
		std mod_edelay

		lda #2
		sta mod_eanim_rate

		lda #$01
		pshs a

30		ldy #monsters
		ldu #tbl_eanim

40		bsr eanim_add
		pshs y,u

		lda 4,s
		deca
		beq 42F
		cmpa #13
		bne 44F
		sta 4,s
		lda #$ff
		ldx #snd_tport
		jsr psg_submit
		bra 46F
42		lda #$ff
		ldx #snd_magic
		jsr psg_submit
		lda #25
44		sta 4,s
46

		jsr animate
		bsr eanim_step

		jsr psg_play2frags
mod_edelay	equ *+1
		ldx #1200
50		leax -1,x
		bne 50B

		ldx mod_edelay
		leax -2,x
		beq 90F
		stx mod_edelay
		cmpx #900
		beq 60F
		cmpa #600
		bne 62F
60		lda mod_eanim_rate
		adda #2
		sta mod_eanim_rate
62

		puls y,u
		cmpu #tbl_eanim_end
		blo 40B
		bra 30B

90		leas 5,s

		; finish off
		ldb #15
		stb tmp2+1
92		jsr animate
		jsr eanim_step
		jsr psg_play2frags
		dec tmp2+1
		bne 92B

		jmp level_setup

eanim_start
		lda #bones0
		bra 10F
eanim_cont
		lda obj_tile,u
		inca
		cmpa #bones2
		bls 10F
		clra
10		sta obj_tile,u
		ldx obj_woff,u
		ldb obj_x,u
		jsr obj_draw_vx
		lda obj_tile,u
		bne 20F
		lda #$80
		sta obj_y,u
20		lda spr_state,u
mod_eanim_rate	equ *+1
		adda #2
		sta spr_state,u
		rts

		DATA
tbl_eanim
		fcb $16,4
		fcb $16,3
		fcb $16,2
		fcb $16,1
		fcb $15,1
		fcb $14,1
		fcb $13,1
		fcb $13,2
		fcb $13,3
		fcb $13,4
		fcb $14,4
		fcb $15,4
tbl_eanim_end

jtbl_eanim_state
		STATE_JTBL
		STATE_ID "state_eanim_start"
		fdb eanim_start
		fdb spr_delay
		fdb spr_delay
		fdb spr_delay
		fdb eanim_cont
		fdb spr_delay
		fdb spr_delay
		fdb spr_delay
		fdb eanim_cont
		fdb spr_delay
		fdb spr_delay
		fdb spr_delay
		fdb eanim_cont
		fdb spr_nop
		fdb spr_nop
		fdb spr_nop
		CODE


; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; First state while changing room.  Initialises player window with map
; outline: stone & floor.  Next state will draw all objects.

; Faster drawing, as can make more assumptions when doing every tile.

; Entry:
;     X -> screen
;     U -> player

DRAW_ROW_A_FAST	macro
		pulu d
		std \1,x
		endm

DRAW_LAST_ROW_A_FAST	macro
		ldd ,u
		std \1,x
		endm

DRAW_ROW_B_FAST	macro
		lda \1,x
		clrb
		addd ,u++
		std \1,x
		endm

DRAW_LAST_ROW_B_FAST	macro
		lda \1,x
		clrb
		addd ,u
		std \1,x
		endm

plr_draw_room
		ldb spr_next_y,u
		andb #$f8
		ldx #bmp_wall+128
		leay b,x
		ldx plr_win,u
		lda #win_h
		sta tmp1
		pshs u
10		lda ,y+
		sta tmp0
		lda #win_w/2
		sta tmp1+1

		; even tiles
20		ldu #tile_floor_a
		lsl tmp0
		bcc 30F
mod_stone_a	equ *+1
		ldu #$0000
30		DRAW_ROW_A_FAST -128
		DRAW_ROW_A_FAST -96
		DRAW_ROW_A_FAST -64
		DRAW_ROW_A_FAST -32
		DRAW_ROW_A_FAST 0
		DRAW_ROW_A_FAST 32
		DRAW_ROW_A_FAST 64
		DRAW_ROW_A_FAST 96
		DRAW_LAST_ROW_A_FAST 128

		; odd tiles
		leax 1,x
		ldu #tile_floor_b
		lsl tmp0
		bcc 40F
mod_stone_b	equ *+1
		ldu #$0000
40		DRAW_ROW_B_FAST -128
		DRAW_ROW_B_FAST -96
		DRAW_ROW_B_FAST -64
		DRAW_ROW_B_FAST -32
		DRAW_ROW_B_FAST 0
		DRAW_ROW_B_FAST 32
		DRAW_ROW_B_FAST 64
		DRAW_ROW_B_FAST 96
		DRAW_LAST_ROW_B_FAST 128

		leax 2,x
		dec tmp1+1
		lbne 20B

		leax (9*fb_w)-12,x
		dec tmp1
		lbne 10B
		puls u
		jmp spr_inc_state

DRAW_ROW_A	macro
		clra
		ldb (\1+1),x
		andb #$0f
		addd ,u++
		std \1,x
		endm

DRAW_LAST_ROW_A	macro
		clra
		ldb (\1+1),x
		andb #$0f
		addd ,u
		std \1,x
		endm

DRAW_ROW_B	macro
		lda \1,x
		clrb
		anda #$f0
		addd ,u++
		std \1,x
		endm

DRAW_LAST_ROW_B	macro
		lda \1,x
		clrb
		anda #$f0
		addd ,u
		std \1,x
		endm

single_tile_a	ldu #tbl_tiles_a
		leau a,u
		ldu a,u
single_tile_x_a
		DRAW_ROW_A -128
		DRAW_ROW_A -96
		DRAW_ROW_A -64
		DRAW_ROW_A -32
		DRAW_ROW_A 0
		DRAW_ROW_A 32
		DRAW_ROW_A 64
		DRAW_ROW_A 96
		DRAW_LAST_ROW_A 128
		rts

draw_object_once
		ldb obj_x,y
		lsrb
		bcc single_tile_a
		; fall through to single_tile_b

single_tile_b	ldu #tbl_tiles_b
		leau a,u
		ldu a,u
single_tile_x_b
		DRAW_ROW_B -128
		DRAW_ROW_B -96
		DRAW_ROW_B -64
		DRAW_ROW_B -32
		DRAW_ROW_B 0
		DRAW_ROW_B 32
		DRAW_ROW_B 64
		DRAW_ROW_B 96
		DRAW_LAST_ROW_B 128
99		rts

update_chalice
		ldu #player1
		bsr plr_update_chalice
		ldu #player2
		bsr plr_update_chalice
		ldu #player3
		bsr plr_update_chalice
		ldu #player4
		; fall through to plr_update_chalice

plr_update_chalice
		ldb plr_yroom,u
plr_update_chalice_b
		andb #$f8
		cmpb #$10
		bne 99B
10		ldx plr_win,u
		leax 4*9*fb_w+3,x
mod_chalice	equ *+1
		ldu #tile_chalice_nw0_a
		jsr single_tile_x_a
		leau 2,u
		leax 1,x
		jsr single_tile_x_b
		leau 2,u
		leax 9*fb_w-1,x
		jsr single_tile_x_a
		leau 2,u
		leax 1,x
		jmp single_tile_x_b

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		setdp 0			; assumed
game_reset	nop

		orcc #$50
		lds #$0100

		lda #$ff
		tfr a,dp
		setdp $ff

		; A == $ff from above
		clr reg_pia0_cra	; $00 - DDR
		clr reg_pia0_crb	; $00 - DDR
		clr reg_pia1_cra	; $00 - DDR
		clr reg_pia1_crb	; $00 - DDR
		ldb #$35		; $35 - PDR, FS hi->lo
		std reg_pia0_ddrb	; $ff - all outputs
		clra			; $00 - all inputs
		decb			; $34 - PDR, no HS
		std reg_pia0_ddra
		lda #$fc		; $fc - only DAC bits as outputs
		std reg_pia1_ddra	; $34 - PDR, no printer FIRQ
PATCH_p1ddrb	equ *+1
		ldd #$fc3c		; $fc - VDG, ROMSEL as outputs
		std reg_pia1_ddrb	; $3c - PDR, no CART FIRQ

		; SAM MPU rate = slow
		sta reg_sam_r1c
		sta reg_sam_r0c

		; SAM display offset = $0200
		std reg_sam_f0s		; and f1c
		sta reg_sam_f2c

		; SAM VDG mode = G6R,G6C
		sta reg_sam_v0c
		sta reg_sam_v1s
		sta reg_sam_v2s

		; Always sit in map type 1
		sta reg_sam_tys

		lda #gamedp
		tfr a,dp
		setdp gamedp

		lda level
		inca
		beq 10F
		ldd #$fd40
		sta reg_pia0_pdrb
		bitb reg_pia0_pdra	; CLEAR?
		lbeq cheat
10

show_title_screen
mod_show_title	bra 20F
		lda #$e2
		sta reg_pia1_pdrb	; VDG mode CG6, CSS0
		ldx #fb0
10		ldd #$aaaa
		std ,x++
		cmpx #fb0_end
		blo 10B
		ldu #fb0+10*fb_w
		ldd #fb0+10*fb_w+151*fb_w
		std dzip_end
		; XXX MAGIC - $8500 is where dzipped title screen is
		; copied to by loader
		ldx #$8500
		jsr dunzip
		clr text_colour
		ldd #171*256+41
		ldy #press_key
		jsr write_at
		jsr reset_song
		jsr play_song
20

show_select_screen

		; three times around the select/credits screens before
		; returing to title
		lda #3
		sta tmp2

redraw_select_screen

PATCH_vdgmode_select_screen	equ *+1
		lda #$e2
		sta reg_pia1_pdrb	; VDG mode CG6, CSS0

		jsr unpack_text_screen

		ldb colour_fg
		stb text_colour
		ldu #textlist_select_screen
		jsr write_textlist
		lda colour_hi
		sta text_colour
		jsr write_charlist

		jsr update_space
		jsr update_p1
		jsr update_p2
		jsr update_p3
		jsr update_p4
		jsr update_vmode

		ldd #$0a0a
		std player1+spr_tmp0
		std player2+spr_tmp0
		std player3+spr_tmp0
		std player4+spr_tmp0

select_loop
		jsr wait_keyheld

		ldd #$0600	; about 30s delay
		std tmp0	; until next screen

select_anim_loop

		; Selected players are animated, showing them moving in
		; one direction for a while, then moving in another.

		; spr_tmp0,u is the inter-frame animation counter
		; spr_tmp0+1,u counts between updates to facing direction

		ldx #select_anim
		ldu #players
10		lda plr_bit,u
		bita playing
		beq 90F
		dec spr_tmp0,u
		bne 90F

		lda #10
		sta spr_tmp0,u
		lda spr_tilebase,u
		eora #1
		sta spr_tilebase,u

		dec spr_tmp0+1,u
		bne 90F
		lda #10
		sta spr_tmp0+1,u
		lda spr_facing,u
		lsra
		lda a,x
		sta spr_facing,u

90		leau sizeof_plr,u
		cmpu #players_end
		blo 10B

		; delay ~1/50s
		ldd #2400
10		subd #1
		bne 10B

		ldu #players
		ldx #fb0+53*fb_w+4+128
10		lda spr_tilebase,u
		adda spr_facing,u
		pshs x,u
		jsr single_tile_b
		puls x,u
		leax 14*fb_w,x
		leau sizeof_plr,u
		cmpu #players_end
		blo 10B

		ldx #tbl_kdsp_menu
		jsr test_keypress_jump

		dec tmp0+1
		bne select_anim_loop
		dec tmp0
		bne select_anim_loop
		; fall through to show_credits

		; - - - -

		; Credits screen 1
show_credits	ldb colour_fg
		stb text_colour
		jsr unpack_text_screen
		ldy #credits_screen
		jsr write_screen

		bsr credits_wait
		lbne show_select_screen

		; - - - -

		; Credits screen 2
		inc mod_line_height
		jsr unpack_text_screen
		ldy #credits_screen2
		jsr write_screen
		dec mod_line_height

		bsr credits_wait
		lbne show_select_screen

		; - - - -

		; Return either to select screen or title screen.
		dec tmp2
		lbne redraw_select_screen
		jmp show_title_screen

		; - - - -

; Wait around 20s per credits screen
credits_wait	ldx #1000
10		ldd #2540
20		subd #1
		bne 20B
		leax -1,x
		beq 99F
		jsr test_keypressed
		beq 10B
99		rts

toggle_p1	lda #$01
		bsr toggle_player
		bsr update_p1
		jmp select_loop
toggle_p2	lda #$02
		bsr toggle_player
		bsr update_p2
		jmp select_loop
toggle_p3	lda #$04
		bsr toggle_player
		bsr update_p3
		jmp select_loop
toggle_p4	lda #$08
		bsr toggle_player
		bsr update_p4
		jmp select_loop
toggle_player	eora playing
		sta playing
		bsr update_space
		rts

update_p1	lda #$01
		ldx #fb0+53*fb_w+16+128
		bra update_player
update_p2	lda #$02
		ldx #fb0+67*fb_w+16+128
		bra update_player
update_p3	lda #$04
		ldx #fb0+81*fb_w+16+128
		bra update_player
update_p4	lda #$08
		ldx #fb0+95*fb_w+16+128
update_player	anda playing
		bne draw_yes
draw_no		ldu #tile_no_b
		bra 10F
draw_yes	ldu #tile_yes_b
10		jmp single_tile_x_b

toggle_skill	bsr undraw_skill
		com difficulty
		bsr update_skill
		jmp select_loop
toggle_vmode	jsr advance_video_mode
		jmp show_select_screen

; Changing video mode means redrawing screen, no need for "undraw".

update_vmode	lda colour_fg
		sta text_colour
		lda video_mode
		lsla
		ldx #tbl_text_vmode
		ldy a,x
		ldd #132*256+62
		jsr write_at
		; only called in one place, and update_skill required
		; immediately after, so...
		; fall through to update_skill

update_skill	lda colour_fg
		bra 10F
undraw_skill	lda colour_bg
10		sta text_colour
		ldd #118*256+62
		tst difficulty
		beq 20F
		ldy #text_hard
		bra 30F
20		ldy #text_easy
30		jmp write_at

; "<Space> to play" is only highlighed when a player is selected.
update_space	lda playing
		bne 10F
		lda colour_fg
		bra 20F
10		lda colour_hi
20		sta text_colour
		ldd #160*256+32
		ldy #text_space
		jmp write_at

		DATA
; Select screen character animation: next "facing" based on current so
; that animation is in order: up, right, down, left.
select_anim	fcb 6,4,0,2

tbl_text_vmode	fdb text_pal
		fdb text_ntsc0
		fdb text_ntsc1

tbl_kdsp_menu	KEYDSP_ENTRY 7,5,start_game	; SPACE
		KEYDSP_ENTRY 1,0,toggle_p1	; '1'
		KEYDSP_ENTRY 2,0,toggle_p2	; '2'
		KEYDSP_ENTRY 3,0,toggle_p3	; '3'
		KEYDSP_ENTRY 4,0,toggle_p4	; '4'
		KEYDSP_ENTRY 4,2,toggle_skill	; 'D'
		KEYDSP_ENTRY 6,4,toggle_vmode	; 'V'
		KEYDSP_ENTRY 2,6,game_reset	; BREAK
		fdb 0
		CODE

		; - - - - - - - - - - - - - - - - - - - - - - -

		DATA
easy_patchset	PATCH mod_skill_door
		fcb $02		; alternate door graphic
		ENDPATCH
		PATCH mod_skill_key
		fcb $4c		; alternate key graphic
		ENDPATCH
		PATCH mod_drainer_hp
		fcb $09		; lower drainer strength
		ENDPATCH
		PATCH mod_monster_hp
		fcb $04		; half initial hp
		ENDPATCH
		PATCH mod_mon_hit_player
		fcb $92		; 8-armour damage -> 100-8(BCD)
		ENDPATCH
		PATCH mod_drainer_dmg
		fcb $90		; 10 damage -> 100-10(BCD)
		ENDPATCH
		ENDPATCHSET

hard_patchset	PATCH mod_skill_door
		fcb $00		; don't alternate door graphic
		ENDPATCH
		PATCH mod_skill_key
		fcb $12		; don't alternate key graphic
		ENDPATCH
		PATCH mod_drainer_hp
		fcb $0c		; full drainer strength
		ENDPATCH
		PATCH mod_monster_hp
		fcb $08		; full initial hp
		ENDPATCH
		PATCH mod_mon_hit_player
		fcb $90		; 10-armour damage -> 100-10(BCD)
		ENDPATCH
		PATCH mod_drainer_dmg
		fcb $80		; 20 damage -> 100-20(BCD)
		ENDPATCH
		ENDPATCHSET
		CODE

start_game

		; must select some players
		lda playing
		lbeq select_loop

		ldu #hard_patchset
		lda difficulty
		bne 10F
		ldu #easy_patchset
10		jsr apply_patchset

		; flag all players as "dead" so they get reset
		lda #$ff
		sta dead

	if DEBUG_LEVEL25

		bra 30F
cheat		lda #23
30		sta level

	else

		; level -1: will be incremented before use
		sta level
cheat

	endif

		; - - - - - - - - - - - - - - - - - - - - - - -

		; Set up next level.

level_setup

		lda playing
		eora #$0f
		sta finished
		clr exiting

		lda #30
		sta finish_delay

		; Clear monsters
		ldx #monsters
		ldd #$8000|monster_tilebase
10		clr spr_hp,x
		sta obj_y,x
		stb spr_tilebase,x
		leax sizeof_mon,x
		cmpx #monsters_end
		blo 10B

		; Next level.  Detect when game is complete (finished
		; level 25).  Modify various routines to only call
		; chalice-based routines on level 25.  Decode level data.
		ldb #$8c	; cmpx
		inc level
		lda level
		cmpa #24
		beq 10F
		blo 20F
		ldy #end_screen
		jmp game_over_screen
10		ldb #$bd	; jsr
20		stb mod_draw_chalice_0
		stb mod_draw_chalice_1
		stb mod_check_chalice

		ldu #levels
		; level size is 378 but we can't multiply by that...
		ldb #189
		lsla
		mul
		leau d,u
		; copy initial wall bitmap
		ldx #bmp_wall
25		ldd ,u++
		std ,x++
		cmpx #bmp_wall_end
		blo 25B
		; clear object bitmap
30		clr ,x+
		cmpx #bmp_objects_end
		blo 30B

		; Select stone graphic based on level
		lda level
		ldb #18
		mul
		addd #tile_stone01_a
		std mod_stone_a
		addd #tiles_b_start-tiles_a_start
		std mod_stone_b

		; Populate object table

		; source: u left pointing to start of level object list
		ldy #objects		; destination: object table

		; Doors.  Source is (y,x) pairs.  x is shifted left one
		; bit, with the bottom bit then used to indicate
		; horizontal or vertical door.
		clr tmp0+1
		lda #max_doors
		sta tmp0
10		pulu d
		cmpd #$8080
		bne 20F
		; invalid
		tfr d,x
		clra
		bra 40F
		; valid
20		lsrb		; CC.C set => vertical door
		tfr d,x
		lda #door_h
		bcc 30F
		lda #door_v
30		com tmp0+1
		bne 40F
mod_skill_door	equ *+1
		suba #$00	; change to suba #2 to alternate colours
40		bsr place_object
		jsr clr_pickup_x	; not a pickup
		dec tmp0
		bne 10B

		; Keys.  Source is (y,x) of key, and order is important:
		; the nth key here opens the nth door above.
		clr tmp0+1
		lda #max_keys
		sta tmp0
10		pulu x
		clra
		cmpx #$8080
		beq 20F
		lda #key
		com tmp0+1
		bne 20F
mod_skill_key	nop		; change to inca to alternate colours
20		bsr place_object
		; fetch door location from earlier in map data
		ldd -(max_doors*2)-2,u
		lsrb
		std obj_key_opens-sizeof_obj,y
30		dec tmp0
		bne 10B

		; Items (pickups, drainers).  Source is (type,y,x).
		lda #max_items
		sta tmp0
10		pulu a,x
		; ignored for non-drainers
mod_drainer_hp	equ *+1
		ldb #12
		stb obj_drainer_hp,y
		bsr place_object
		dec tmp0
		bne 10B

		; Trapdoors.  Source is (y,x).
		lda #max_trapdoors
		sta tmp0
10		pulu x
		lda #trapdoor
		bsr place_object
		dec tmp0
		bne 10B

		; Clear shots.
		clra
10		bsr place_object
		cmpy #shots_end
		blo 10B

		bra finish_setup

; Set object type & location, mark pickup bitmap if valid (type != 0).

; Entry:
; 	A = object type
; 	X = y,x
; 	Y -> object
place_object
		pshs x
		stx obj_y,y
		sta obj_tile,y
		beq 90F
		tfr x,d
		jsr set_pickup
		jsr yx_to_woff
		stx obj_woff,y
90		leay sizeof_obj,y
		puls x,pc

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Set up players.  If player is dead, restore default stats and clear
; score.  Always restore to 99 health.

init_players

		ldy #player1
		ldu #tbl_p_dfl
		lda #1

10
		sta plr_bit,y

		pulu a,b,x
		sta spr_tilebase,y
		stb spr_facing,y	; initial direction
		stx spr_next_y,y	; incl. spr_next_x
		pulu a,b,x
		std plr_swin,y		; stats window address
		stx plr_win,y		; player window address

		ldd #$8080
		std obj_y,y
		std plr_key_opens,y	; no key
		clr plr_speed,y		; no speedyboots
		ldd #$9909		; init health
		sta spr_hp,y		; init health = 99
		stb plr_yroom,y		; invalidate yroom
		clr plr_nshots,y	; not shooting
		; initial state for all players is to draw room
		ldb #state_plr_draw_room
		stb spr_state,y

		pulu a,x
		lsr dead		; player dead?
		bcc 20F
		; reset player stats if dead
		sta plr_armour,y
		stx plr_power,y		; incl. ammo
		clr plr_score2,y	; clear score
		clr plr_score0,y
20

		lda plr_bit,y
		leay sizeof_plr,y
		lsla
		cmpa #$08
		bls 10B

		rts

		DATA
tbl_p_dfl
		; player 1 defaults
		fcb p1_tilebase		; spr_tilebase
		fcb facing_up		; spr_facing
		fcb 7*8+3-128,3		; spr_next_y,spr_next_x
		fdb p1_swin		; plr_swin
		fdb p1_win+128		; plr_win
		fcb 3			; plr_armour
		fcb 2,3			; plr_power,plr_ammo
		; player 2 defaults
		fcb p2_tilebase		; spr_tilebase
		fcb facing_right	; spr_facing
		fcb 7*8+4-128,3		; spr_next_y,spr_next_x
		fdb p2_swin		; plr_swin
		fdb p2_win+128		; plr_win
		fcb 1			; plr_armour
		fcb 3,2			; plr_power,plr_ammo
		; player 3 defaults
		fcb p3_tilebase		; spr_tilebase
		fcb facing_down		; spr_facing
		fcb 7*8+3-128,4		; spr_next_y,spr_next_x
		fdb p3_swin		; plr_swin
		fdb p3_win+128		; plr_win
		fcb 4			; plr_armour
		fcb 9,1			; plr_power,plr_ammo
		; player 4 defaults
		fcb p4_tilebase		; spr_tilebase
		fcb facing_left		; spr_facing
		fcb 7*8+4-128,4		; spr_next_y,spr_next_x
		fdb p4_swin		; plr_swin
		fdb p4_win+128		; plr_win
		fcb 6			; plr_armour
		fcb 5,2			; plr_power,plr_ammo
		CODE

; Continues from level_setup.

finish_setup
		bsr init_players
		jsr unpack_play_screen

		clra
		clrb

		sta dead		; clear remaining bits
		sta dying		; and dying flags
		ldx #death_fifo		; initialise death fifo
		stx death_fifo_end

		ldx #psg_f1
		stx psg_c1env
		std ,x
		ldx #psg_f2
		stx psg_c2env
		std ,x
		sta psg_c1pri
		sta psg_c2pri

		jmp mainloop

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Player input.  Scan keyboard & joysticks, set plr_control accordingly.

		setdp $ff	; only ever called with DP=$ff

		DATA
tbl_p1_keys	KEYTBL_ENTRY 3,2,2			; 'C' = magic
		KEYTBL_ENTRY 7,4,$80|facing_up		; 'W' = up
		KEYTBL_ENTRY 3,4,$80|facing_down	; 'S' = down
		KEYTBL_ENTRY 0,5,$80|facing_right	; 'X' = right
		KEYTBL_ENTRY 2,5,$80|facing_left	; 'Z' = left
		KEYTBL_ENTRY 1,4,1			; 'Q' = fire
		fdb 0
tbl_p2_keys	KEYTBL_ENTRY 2,2,2			; 'B' = magic
		KEYTBL_ENTRY 7,3,$80|facing_up		; 'O' = up
		KEYTBL_ENTRY 3,3,$80|facing_down	; 'K' = down
		KEYTBL_ENTRY 5,3,$80|facing_right	; 'M' = right
		KEYTBL_ENTRY 6,3,$80|facing_left	; 'N' = left
		KEYTBL_ENTRY 0,4,1			; 'P' = fire
		fdb 0
		CODE

; On exit, B always == 0

scan_keys
10		ldd ,x++
		beq 99F
		sta reg_pia0_pdrb
		lda ,x+
		andb reg_pia0_pdra
		bne 10B
		sta plr_control,u
99		rts

plr_scan_controls

		setdp gamedp		; called with usual game DP

		clra
		sta player1+plr_control
		sta player2+plr_control
		sta player3+plr_control
		sta player4+plr_control

		; keyboard & joystick scanning - will hit the PIAs a lot,
		; so set DP
		coma		; lda #$ff
		tfr a,dp
		setdp $ff

		; player 1 - table-driven scan
		ldu #player1
		ldx #tbl_p1_keys
		bsr scan_keys

		; player 2 - table-driven scan
		ldu #player2
		ldx #tbl_p2_keys
		bsr scan_keys

		decb			; ldb #$ff - scan_keys leaves b==0
		stb reg_pia0_pdrb	; polling firebuttons

		; player 3 - left joystick
		ldu #player3
		lda #$3d
		sta reg_pia0_crb	; left joystick
		lda reg_pia0_pdra
		bita #$02
		bne 10F
		lda #1			; fire
		sta plr_control,u
10		ldb p3_moved
		beq 30F
		bsr joy_yaxis
		bpl 20F
		bsr spr_move_control
		beq 50F
20		bsr joy_xaxis
		bra 50F
30		bsr joy_xaxis
		bpl 40F
		bsr spr_move_control
		beq 50F
40		bsr joy_yaxis
50

		; player 4 - right joystick
		ldu #player4
		lda #$35
		sta reg_pia0_crb	; right joystick
		lda reg_pia0_pdra
		bita #$01
		bne 10F
		lda #1			; fire
		sta plr_control,u
10		ldb p4_moved
		beq 30F
		bsr joy_yaxis
		bpl 20F
		bsr spr_move_control
		beq 50F
20		bsr joy_xaxis
		bra 50F
30		bsr joy_xaxis
		bpl 40F
		bsr spr_move_control
		beq 50F
40		bsr joy_yaxis
50

		; ensure DAC selected when sound reenabled
		ldd #$3400
		sta reg_pia0_cra
		stb reg_pia1_pdra	; zero DAC

		lda #gamedp
		tfr a,dp

		rts

; joy_yaxis and joy_xaxis return with CC.N set if direction is detected.

joy_yaxis	lda #$3c
		sta reg_pia0_cra	; y axis
		ldd #$8040
		sta reg_pia1_pdra
		stb reg_pia1_pdra
		lda reg_pia0_pdra
		bmi 20F
		lda #$80|facing_up	; up
		bra 80F
20		ldd #$ffc0
		sta reg_pia1_pdra
		stb reg_pia1_pdra
		lda reg_pia0_pdra
		bpl 99F
		lda #$80|facing_down	; down
		bra 80F

; just about fits between the two axis checks to make all branches short
spr_move_control
		anda #$7f
		ldx obj_y,u
		pshs x
		ldx spr_next_y,u
		stx obj_y,u
		jsr spr_move_a
		pshs cc
		ldx 1,s
		stx obj_y,u
		puls cc,x,pc

joy_xaxis	lda #$34
		sta reg_pia0_cra	; x axis
		ldd #$8040
		sta reg_pia1_pdra
		stb reg_pia1_pdra
		lda reg_pia0_pdra
		bmi 20F
		lda #$80|facing_left	; left
		bra 80F
20		ldd #$ffc0
		sta reg_pia1_pdra
		stb reg_pia1_pdra
		lda reg_pia0_pdra
		bpl 99F
		lda #$80|facing_right	; right
80		sta plr_control,u
99		rts

		setdp gamedp

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; dunzip - unzip simply coded zip data

; 1iiiiiii 0nnnnnnn - repeat 128-n (1-128) bytes from current + 1i (-ve 2s cmpl)
; 1iiiiiii 1jjjjjjj nnnnnnnn - repeat 128-n (1-256) bytes from current + 1ij
; 0nnnnnnn - directly copy next 128-n (1-128) bytes

; Entry:
; 	X -> zipped data
; 	U -> destination
; 	[dzip_end] = end of zipped data

dunzip
dunz_loop	ldd ,x++
		bpl dunz_run	; run of 1-128 bytes
		tstb
		bpl dunz_7_7
dunz_14_8	lslb		; drop top bit of byte 2
		asra
		rorb		; asrd
		leay d,u
		ldb ,x+
		bra 10F		; copy 1-256 bytes (0 == 256)
dunz_7_7	leay a,u	; copy 1-128 bytes
10		lda ,y+
		sta ,u+
		incb
		bvc 10B		; count UP until B == 128
		bra 30F
20		ldb ,x+
dunz_run	stb ,u+
		inca
		bvc 20B		; count UP until B == 128
30		cmpu <dzip_end
		blo dunz_loop
		rts

; Unpack dzipped screen and draw player stats to it.

unpack_play_screen
		pshs x,y
		ldx #play_screen_dz
		bra 10F
unpack_text_screen
		pshs x,y
		ldx #text_screen_dz
PATCH_vdgmode_unpack_screen	equ *+1
10		lda #$e2	; VDG mode CG6, CSS0
		sta reg_pia1_pdrb

		ldu #fb0
		ldy #fb0+178*fb_w
		sty dzip_end
		clra
		clrb
10		std ,u++
		std ,y++
		cmpu #fb0+14*fb_w
		blo 10B
		bsr dunzip

		ldu #player1
10		bsr plr_draw_stats
		leau sizeof_plr,u
		cmpu #players_end
		blo 10B
		puls x,y,pc

		DATA
play_screen_dz	includebin "play-screen.bin.dz"
text_screen_dz	includebin "text-screen.bin.dz"
		CODE

plr_draw_hp	ldx plr_swin,u
		leax 13*fb_w+128,x
		lda spr_hp,u
		bra draw_lnum_pair

; Entry:
; 	U -> player
plr_draw_stats	pshs y,u
		ldx plr_swin,u
		leax fb_w+128,x
		lda spr_tilebase,u
		adda #2
		jsr single_tile_a
		ldu 2,s
		jsr plr_draw_armour
		jsr plr_draw_power
		bsr plr_draw_ammo
		bsr plr_draw_hp
		bsr plr_draw_score
		puls y,u,pc

plr_add_score
		adda plr_score0,u
		daa
		sta plr_score0,u
		lda plr_score2,u
		adca #$00
		daa
		sta plr_score2,u
		; fall through to plr_draw_score

plr_draw_score	ldx plr_swin,u
		leax 23*fb_w+128,x
		lda plr_score2,u
		bsr draw_lnum_pair
		lda plr_score0,u
		; fall through to draw_lnum_pair

draw_lnum_pair
		pshs a
		lsra
		lsra
		lsra
		bsr 10F
		puls a
		lsla
10		anda #$1e
		pshs u
		ldu #tbl_lnum
		ldu a,u
		pulu d
		sta -128,x
		stb -96,x
		pulu d
		sta -64,x
		stb -32,x
		pulu d
		sta ,x+
		stb 31,x
		lda ,u
		sta 63,x
		puls u,pc

plr_draw_ammo	ldx plr_swin,u
		leax 15*fb_w+2,x
		lda plr_ammo,u
		sta tmp1
		suba #4
		sta tmp1+1
PATCH_draw_ammo0	equ *+1
		ldd #$55ba
		stb 1,x
		leax fb_w,x
PATCH_draw_ammo1	equ *+1
10		ldb #$7e
		std ,x
PATCH_draw_ammo2	equ *+1
		ldb #$ba
		stb fb_w+1,x
		leax 2*fb_w,x
		dec tmp1
		bne 10B
PATCH_draw_ammo3	equ *+1
		ldd #$aaaa
20		inc tmp1+1
		beq 99F
		std ,x
		std fb_w,x
		leax 2*fb_w,x
		bra 20B
99		rts

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Monster and player state machine handling.

; Monsters.  First, spawn a new monster if a slot is available, then for
; each extant monster, run its state machine.

process_monsters
		bsr new_monster
mon_run_state
		ldu #monsters
10		lda obj_y,u
		deca
		bvs 20F
		lda spr_state,u
		ldx #jtbl_monster_state
		jsr [a,x]
20		leau sizeof_mon,u
		cmpu #monsters_end
		blo 10B
		rts

		DATA
jtbl_monster_state
		STATE_JTBL
		fdb mon_centred
		fdb mon_check_direction
		fdb spr_move1
		fdb spr_delay
		fdb spr_move2
		STATE_ID "state_mon_landed"
		fdb mon_landed
		STATE_ID "state_mon_new_room"
		fdb spr_delay
		fdb spr_delay
		fdb spr_delay
		STATE_ID "state_mon_landed2"
		fdb mon_landed
		; keep these as the last states
		STATE_ID "state_mon_die"
		fdb mon_die
		fdb mon_dying
		CODE

; Players.

process_players
		lda level
		cmpa #24
		bne 10F
		; special-case finishing level 25
		lda playing
		eora #$0f
		sta finished
10		ldu #players
20		lda spr_state,u
		bmi 30F
		lda spr_state,u
		ldx #jtbl_player_state
		jsr [a,x]
30		leau sizeof_plr,u
		cmpu #players_end
		blo 20B
		rts

		DATA
jtbl_player_state
		STATE_JTBL
		STATE_ID "state_plr_centred"
		fdb plr_centred
		fdb plr_delay
		fdb plr_delay
		fdb plr_delay
		fdb plr_landed_nu
		STATE_ID "state_plr_move1"
		fdb spr_move1
		STATE_ID "state_plr_move2"
		fdb spr_move2
		STATE_ID "state_plr_landed"
		fdb plr_landed
		STATE_ID "state_plr_draw_room"
		fdb plr_draw_room
		STATE_ID "state_plr_draw_objects"
		fdb plr_draw_objects
		; keep these as the last states
		STATE_ID "state_plr_inactive"
		fdb plr_inactive
		STATE_ID "state_plr_exit0"
		fdb plr_exit0
		STATE_ID "state_plr_exitdelay"
		fdb spr_delay
		fdb plr_exit1
		STATE_ID "state_plr_die"
		fdb plr_die
		STATE_ID "state_plr_dying_delay"
		fdb spr_delay
		STATE_ID "state_plr_dying"
		fdb plr_dying
		STATE_ID "state_plr_undying_delay"
		fdb spr_delay
		STATE_ID "state_plr_undying"
		fdb plr_undying
		CODE

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Monster state handlers.

; Find next empty monster slot (if any) and create a new one.  Successive
; calls iterate through each trapdoor in turn.

new_monster
next_td		equ *+2
		ldy #trapdoors
		leau sizeof_obj,y
		cmpu #trapdoors_end
		blo 10F
		ldu #trapdoors
10		stu next_td
		ldu #monsters
20		lda obj_y,u
		deca
		bvs 30F
		leau sizeof_mon,u
		cmpu #monsters_end
		blo 20B
		rts
30		lda level		; XXX monster strength by level?
		lsra
mod_monster_hp	equ *+1
		adda #8
		sta spr_hp,u
		ldd obj_y,y
		std obj_y,u
		stb spr_cx,u
		jsr yx_to_woff
		stx obj_woff,u
		stx spr_owoff,u
		; fall through to mon_change_direction

; Choose next "random" direction for a monster to head in.  Actually, it
; simply cycles through the directions, but as it may be called for
; multiple monsters each in turn, can appear random.

mon_change_direction
next_dir	equ *+1
		lda #$06
10		adda #2
		anda #$06
		cmpa spr_facing,u	; same direction?
		beq 10B			; try again
		sta next_dir
		sta spr_facing,u
		clr spr_state,u
		; fall through to spr_set_tile

; Determine which tile to use for a sprite.  Calculates based on facing, x
; and y and adds to tilebase.  Because x & y vary, this animates sprite
; as it moves.

spr_set_tile
		lda spr_facing,u
		ldb obj_y,u
		eorb obj_x,u
		lsrb
		adca spr_tilebase,u
		sta obj_tile,u
		rts

mon_check_direction
		bsr spr_move
		bne 60F
		pshs u
		ldu #players
10		cmpd obj_y,u
		beq 40F			; monster hit player
		leau sizeof_plr,u
		cmpu #players_end
		blo 10B
		ldu #monsters
20		cmpd obj_y,u
		beq 50F
		leau sizeof_mon,u
		cmpu #monsters_end
		blo 20B
		; no collisions - monster is moving
		puls u
		std spr_next_y,u
		eora obj_y,u
		anda #$f8
		beq 30F
		bsr spr_set_tile
		lda #state_mon_new_room
		sta spr_state,u
		rts
30		bsr spr_set_tile
		jmp spr_inc_state
		; monster hit player
40		lda plr_bit,u
		bita dying
		bne 50F
		lda #1
		ldx #snd_hit
		jsr psg_submit
mod_mon_hit_player	equ *+1
		lda #$90		; decrement hp by 10-armour
		adda plr_armour,u
		jsr plr_damage
		; collision - change direction
50		puls u
60		bsr mon_change_direction
		jmp obj_draw

; Test moving a sprite based on 'facing'.  Belongs with other
; sprite-generic functions, but siting here saves some space.

; Entry:
; 	A = pre-loaded 'facing' (spr_move_a only)
; 	U -> sprite
; Returns:
; 	A,B = new y,x
; 	X -> stone bitmap byte
; 	CC.Z = 1 if not stone/door

; WARNING: called from player key scanning routine, which sets DP=$ff.  Ok
; for now as no DP vars are accessed, but be careful modifying.

spr_move
		lda spr_facing,u
spr_move_a
		ldx #jtbl_spr_move
		jmp [a,x]
		DATA
jtbl_spr_move	fdb spr_move_up,spr_move_down
		fdb spr_move_left,spr_move_right
		CODE
spr_move_up	ldd obj_y,u
		deca
		bra 10F
spr_move_down	ldd obj_y,u
		inca
10		ldx #tbl_x_to_bit
		pshs b
		ldb b,x
		ldx #bmp_wall+128
		bitb a,x
		puls b,pc
spr_move_left	ldd obj_y,u
		decb
		bpl 10B
		ldb #$07
		suba #48
		bra 10B
spr_move_right	ldd obj_y,u
		incb
		andb #7
		bne 10B
		adda #48
		bra 10B

mon_landed
		jsr spr_undraw
		ldd spr_next_y,u
		std obj_y,u
		stb spr_cx,u
		jsr yx_to_woff
		stx obj_woff,u
		stx spr_owoff,u
		clr spr_state,u
		; fall through to mon_centred

mon_centred
		jsr spr_set_tile
		bsr obj_draw
		bra spr_inc_state

mon_die
		lda #bones0
		bsr 10F
		bra spr_inc_state
mon_dying
		lda obj_tile,u
		beq 20F
		inca
		cmpa #bones2
		bhi 20F
10		sta obj_tile,u
		ldx obj_woff,u
		; use cx to position the death anim exactly where the
		; monster was
		ldb spr_cx,u
		bra obj_draw_vx
20		jsr spr_undraw
		ldd spr_next_y,u
		cmpd obj_y,u
		beq 30F
		std obj_y,u
		jsr yx_to_woff
		jsr spr_undraw_v
30		clr obj_tile,u
		ldb #$80
		stb obj_y,u
		rts

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Shared sprite routines.  Common to monsters & players.

		DATA
		; offset,tileset select
tbl_obj_woff1	fcb -3*fb_w,0	; up, x&1=0
		fcb -3*fb_w,1	; up, x&1=1
		fcb 3*fb_w,0	; down, x&1=0
		fcb 3*fb_w,1	; down, x&1=1
		fcb -1,1	; left, x&1=0
		fcb 0,0		; left, x&1=1
		fcb 0,1		; right, x&1=0
		fcb 1,0		; right, x&1=1
		; offset only (tileset remains)
		; could just reuse woff1 table and invert x&1
tbl_obj_woff2	fcb -3*fb_w	; up, x&1=0
		fcb -3*fb_w	; up, x&1=1
		fcb 3*fb_w	; down, x&1=0
		fcb 3*fb_w	; down, x&1=1
		fcb 0		; left, x&1=0
		fcb -1		; left, x&1=1
		fcb 1		; right, x&1=0
		fcb 0		; right, x&1=1
		CODE

spr_move1
		jsr spr_undraw
		ldx #tbl_obj_woff1
		ldb obj_x,u
		andb #1
		orb spr_facing,u
		lslb
		ldd b,x
		; A = offset from current screen position
		; B = tile set select (0=A, 1=B)
		ldx spr_owoff,u
		leax a,x
		stx obj_woff,u
		stb spr_cx,u
		bsr obj_draw_vx
		bra spr_inc_state

spr_inc_state	lda spr_state,u
spr_delay	adda #2
		sta spr_state,u
spr_nop
99		rts

spr_move2
		jsr spr_undraw
		ldx #tbl_obj_woff2
		ldb obj_x,u
		stb spr_cx,u
		andb #1
		orb spr_facing,u
		lda b,x
		; A = offset from current screen position
		; B = tile set select (0=A, 1=B)
		ldx obj_woff,u
		leax a,x
		stx obj_woff,u
		bsr obj_draw_v
		bra spr_inc_state

spr_draw_cx
		ldx obj_woff,u
		ldb spr_cx,u
		bra obj_draw_vx

; Draw object.
; Entry:
; 	U -> object
obj_draw	ldx obj_woff,u
		; XXX this is accounting for the fudged shot delay
		; is there a better way to do this?
		bmi 99B
; ... if X already == woff
obj_draw_v	ldb obj_x,u
; ... if B *also* already == x
obj_draw_vx	lda obj_y,u
		anda #$f8
		sta tmp0		; preserve room
		lda obj_tile,u
; ... if A *also* already == tile, but note must manually set tmp0=yroom
; don't need U to point to an object to call this
obj_draw_vxt	stx tmp1		; preserve woff
obj_draw_vxt1	pshs u
		lsrb
		bcs 10F
		ldx #tbl_tiles_a
		ldy #single_tile_x_a
		bra 20F
10		ldx #tbl_tiles_b
		ldy #single_tile_x_b
20		leax a,x
		ldu a,x
		pshs u
		lda tmp0		; re-fetch room
		cmpa player1+plr_yroom
		bne 30F
		ldx tmp1		; re-fetch woff
		leax p1_win+128,x
		jsr ,y
		lda tmp0		; re-fetch room
30		cmpa player2+plr_yroom
		bne 40F
		ldx tmp1		; re-fetch woff
		leax p2_win+128,x
		ldu ,s
		jsr ,y
		lda tmp0		; re-fetch room
40		cmpa player3+plr_yroom
		bne 50F
		ldx tmp1		; re-fetch woff
		leax p3_win+128,x
		ldu ,s
		jsr ,y
		lda tmp0		; re-fetch room
50		cmpa player4+plr_yroom
		bne 90F
		ldx tmp1		; re-fetch woff
		leax p4_win+128,x
		ldu ,s
		jsr ,y
90		puls x,u,pc

; Undraw sprite.  Sprite's current map position isn't updated until
; movement finishes ("lands"), so during movement, this will only redraw
; the tile the sprite is moving *from*.
; Entry:
; 	U -> sprite

spr_undraw
		ldx spr_owoff,u		; original (centred) woff
spr_undraw_v
		stx tmp1		; preserve woff
		jsr obj_test_pickup
		beq 30F
		ldy #pickups
		ldx obj_y,u
10		lda obj_tile,y
		cmpa #always_draw_tile	; this tile always drawn?
		bhs 20F			; skip - drawn soon enough anyway
		cmpx obj_y,y
		beq 40F
20		leay sizeof_obj,y
		cmpy #objects_end
		blo 10B
		rts
30		clra
40		ldb obj_y,u
		andb #$f8
		stb tmp0
		ldb obj_x,u
		jmp obj_draw_vxt1

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Player state handlers.

; Second state while changing room.  Draws all objects once.
; Subsequently, keys and animated objects are redrawn, anything else is
; only redrawn as part of undrawing a sprite.

plr_draw_objects
		bsr spr_undraw		; from other windows
		ldd plr_win,u
		std 30F
		ldb spr_next_y,u
		stb 20F
		pshs u
mod_draw_chalice_1
		jsr plr_update_chalice_b
		; draw objects
		ldy #objects
10		lda obj_tile,y
		beq 40F
		ldb obj_y,y
20		equ *+1
		eorb #$00
		andb #$f8
		bne 40F
		ldx obj_woff,y
30		equ *+2
		leax >$0000,x
		jsr draw_object_once
40		leay sizeof_obj,y
		cmpy #objects_end
		blo 10B
		puls u
		lda plr_bit,u
		bita playing
		beq plr_set_inactive
		ldb #state_plr_landed
		stb spr_state,u
		bsr plr_update_pos
		lda plr_bit,u
		bita dying
		lbne plr_kill
		rts

; Still need to update yroom for inactive players, as their windows get
; updated anyway.  However, player setup invalidates their (y,x) and the
; inactive state doesn't update this, so checks against their position
; will fail.

plr_set_inactive
		lda #state_plr_inactive
		sta spr_state,u
		lda spr_next_y,u
		anda #$f8
		sta plr_yroom,u
		rts

plr_update_pos	ldd spr_next_y,u
		std obj_y,u
		anda #$f8
		sta plr_yroom,u
		ldd obj_y,u
		stb spr_cx,u
		bsr yx_to_woff
		stx obj_woff,u
		stx spr_owoff,u
		rts

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Entry:
; 	A,B = y,x
; Returns:
; 	X = window offset
yx_to_woff
		pshs a,b,u
		ldu #tbl_y_to_woff
		anda #7
		lsla
		ldx a,u
		ldu #tbl_x_to_woff
		lda b,u
		leax a,x
		puls a,b,u,pc

		DATA
		; vertical offsets within a window
tbl_y_to_woff	fdb $0000,$0120
		fdb $0240,$0360
		fdb $0480,$05a0
		fdb $06c0,$07e0
		; horizontal offsets within a window, and within the whole
		; screen (when writing menus, messages, etc.)
tbl_x_to_woff	fcb 0,1,3,4
		fcb 6,7,9,10
		fcb 12,13,15,16
		fcb 18,19,21,22
		fcb 24,25,27,28
		fcb 30
		CODE

		; - - - - - - - - - - - - - - - - - - - - - - -

; Entry:
; 	U -> sprite
; Returns:
; 	A = y
; 	B = x bit
; 	X -> pickup bitmap byte
; 	CC.Z = 1 if no pickup
obj_test_pickup
		ldd obj_y,u
obj_test_pickup_d
		ldx #tbl_x_to_bit
		ldb b,x
		ldx #bmp_objects+128
		bitb a,x
		rts

		; - - - - - - - - - - - - - - - - - - - - - - -

plr_landed
		jsr spr_undraw
plr_landed_nu
		bsr plr_update_pos
		clr spr_state,u
		bsr obj_test_pickup_d
		beq 90F
		; pickups...
		ldx obj_y,u
		ldy #pickups
10		lda obj_tile,y
		beq 30F
		bmi 30F
		cmpx obj_y,y
		bne 30F
		pshs a,x,y
		; pickups MUST have 0 < obj_tile <= $3f
		ldx #jtbl_pickup
		lsla
		jsr [a,x]
		puls a
		ldx #tbl_pickup_scores
		lda a,x
		beq 20F
		ldy 2,s
		jsr obj_clr_pickup
		clr obj_tile,y
		jsr plr_add_score
20		puls x,y
30		leay sizeof_obj,y
		cmpy #pickups_end
		blo 10B
		; fall through to plr_centred
90

plr_centred
		jsr spr_set_tile
		lda plr_control,u
		beq 50F
plr_test_fire	bpl plr_fire

		anda #$7f
		sta spr_facing,u
		jsr spr_set_tile	; yes, again
		lda dying
		sta tmp0
		jsr spr_move
		beq 10F
mod_check_chalice
		jsr check_chalice
		cmpd plr_key_opens,u
		bne 50F
		jmp plr_open_door
10		lsr tmp0
		bcs 20F
		cmpd player1+obj_y
		beq 50F
20		lsr tmp0
		bcs 30F
		cmpd player2+obj_y
		beq 50F
30		lsr tmp0
		bcs 40F
		cmpd player3+obj_y
		beq 50F
40		lsr tmp0
		bcs 60F
		cmpd player4+obj_y
		bne 60F
50		jsr obj_draw
		jmp spr_inc_state

60		ldy #monsters
70		cmpd obj_y,y
		beq plr_bump
		leay sizeof_mon,y
		cmpy #monsters_end
		blo 70B

		std spr_next_y,u
		anda #$f8
		cmpa plr_yroom,u
		beq 80F
		dec plr_yroom,u	; invalidate yroom
		lda #state_plr_draw_room
		sta spr_state,u
		rts
80		jsr obj_draw
		ldb #state_plr_move1
		lda plr_speed,u
		anda #$7f
		beq 90F
		ldb #state_plr_landed
90		stb spr_state,u
		bra plr_move1

plr_delay
		lda plr_control,u
		bne plr_test_fire
		jmp spr_inc_state

; Entry:
; 	A = fire control (1=>fire, 2=>magic)
; 	U -> player
plr_fire
		lsra
		bne plr_magic
10		lda plr_nshots,u
		cmpa plr_ammo,u
		bhs 50B
		inca
		sta plr_nshots,u
		lda obj_tile,u
		eora #$01
		sta obj_tile,u
		ldy #shots
20		lda obj_tile,y
		beq 30F
		leay sizeof_obj,y
		bra 20B		; XXX *should* be safe...
30		ldb spr_facing,u
		stb obj_shot_facing,y
		lsrb
		addb #8
		addb spr_tilebase,u
		stb obj_tile,y
		ldd obj_y,u
		std obj_y,y
		ldd #$fffe	; 1 frame delay
		std obj_woff,y
		stu obj_shot_plr,y

		lda #2		; priority
		ldx #snd_fire
		jsr psg_submit

		jmp obj_draw	; draw player

		; player bumped into monster
		; limit amount of damage caused by only dropping health if
		; monster is "landed"
plr_bump	lda spr_state,y
		cmpa #state_mon_landed
		beq 10F
		cmpa #state_mon_landed2
		beq 10F
		rts
10		lda #1
		ldx #snd_bump
		jsr psg_submit
		lda #$99		; decrement hp by 1
		jmp plr_damage

		; Continues on from plr_centred, decided player is moving,
		; no speedyboots.  Hack to store decision for
		; joystick-controlled players.
plr_move1	ldb spr_facing,u
		andb #$04	; only care about axis
		lda plr_bit,u
		cmpa #$04
		bne 10F
		stb p3_moved
		bra 99F
10		cmpa #$08
		bne 99F
		stb p4_moved
99		rts

plr_magic
		jsr obj_draw
		lda #$94	; -6HP for player 1
		ldb plr_bit,u
		lsrb
		beq 10F
		lda #$97	; -3HP for player 2
10		jsr plr_damage
		; only kill monsters in player's window
		lda obj_y,u
		sta tmp0+1
		; calculate score and mark monsters for death
		ldy #monsters
20		lda obj_y,y
		eora tmp0+1
		anda #$f8
		bne 30F		; only monsters in the right room
		ldd #$0100|state_mon_die
		stb spr_state,y
		jsr plr_add_score
30		leay sizeof_mon,y
		cmpy #monsters_end
		blo 20B
		lda #$ff	; highest priority
		ldx #snd_magic
		jsr psg_submit
		; step monsters through death and play magic sound fx
		bsr magic_mon_step
		bsr magic_mon_step
		bsr magic_mon_step
		; fall through to magic_mon_step

magic_mon_step	pshs u
		bsr mon_step
		; funny sound
		jsr psg_play2frags
		jsr psg_play2frags
		jsr psg_play2frags
		jsr psg_play2frags
		puls u,pc

mon_step	pshs u
		ldu #monsters
10		lda obj_y,u
		eora tmp0+1
		anda #$f8
		bne 20F		; only monsters in the right room
		lda spr_state,u
		ldx #jtbl_monster_state
		jsr [a,x]
20		leau sizeof_mon,u
		cmpu #monsters_end
		blo 10B
		puls u,pc

; Entry:
; 	A,B = y,x of held key = intended player destination
; 	U -> player
plr_open_door
		ldx #doors
10		cmpd obj_y,x
		beq 20F
		leax sizeof_obj,x
		bra 10B		; XXX should be safe loop
20		clr obj_tile,x
		pshs a,b,u
		leau ,x
		jsr obj_draw
		puls a,b,u
		; clear appropriate bit in wall bitmap
		ldx #tbl_x_to_bit
		ldb b,x
		ldx #bmp_wall+128
		comb
		andb a,x
		stb a,x
		; invalidate player's held key
		ldd #$8080
		std plr_key_opens,u
		lda #3		; priority
		ldx #snd_door
		jsr psg_submit
		lda #7
		jsr plr_add_score
		; consider setting state such that there's a slight delay
		; before moving on?
		; fall through to plr_undraw_key

plr_undraw_key	ldx plr_swin,u
		leax 2+32,x
PATCH_undraw_key	equ *+1
		ldd #$aaaa
		sta -32,x
		stb ,x
		sta 32,x
plr_inactive	rts

		; if player had started to move to another square
		; (spr_next_y != obj_y), undraw that square
plr_die		ldd spr_next_y,u
		cmpd obj_y,u
		beq 10F
		ldx obj_y,u
		pshs x			; preserve old y,x
		std obj_y,u
		jsr yx_to_woff
		jsr spr_undraw_v
		puls x
		stx obj_y,u		; restore old y,x
		stx spr_next_y,u	; ensure resurrects in same place
10		lda #bones0-1
		sta obj_tile,u
		jmp spr_inc_state

plr_dying	lda #state_plr_dying_delay
		sta spr_state,u
		lda obj_tile,u
		cmpa #bones7
		blo 20F
		ldb plr_bit,u
		bitb dying
		bne 10F
		ldb #state_plr_undying
		stb spr_state,u
		bra 30F
10		orb dead
		stb dead
		fcb $21		; brn - skip inca
20		inca
30		sta obj_tile,u
		ldx spr_owoff,u
		; don't use cx - player should die centred
		ldb obj_x,u
		jmp obj_draw_vx

; Resurrection runs the death animation in reverse.
plr_undying
		lda #state_plr_undying_delay
		sta spr_state,u
		lda obj_tile,u
		cmpa #bones0
		bls 10F
		deca
		bra 30B
10 		lda #state_plr_landed
		sta spr_state,u
		lda #$32
		sta spr_hp,u
		jmp plr_draw_hp

; Test if player is moving into the chalice, and if so set that player's
; bit in 'finished'.  When 'finished' contains all living players, the
; game is won.

; It's ok to destroy A here, as B will still test false in plr_centred,
; and there are no actual doors on level 25.
check_chalice	cmpa #$14
		blo 10F
		cmpa #$15
		bhi 10F
		cmpb #2
		blo 10F
		cmpb #3
		bhi 10F
		lda plr_bit,u
		ora finished
		bra 20F
10		lda plr_bit,u
		coma
		anda finished
20		sta finished
		rts

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Handle player damage and death.  Death may involve dropping a held key,
; so that's here too.

; Sets bit in pickup table.
; Entry:
; 	A = y
; 	B = x
set_pickup
		pshs b,x
		ldx #tbl_x_to_bit
		ldb b,x
		ldx #bmp_objects+128
		orb a,x
		stb a,x
		puls b,x,pc

; Entry:
; 	A = number to add to player hp (100-damage in BCD)
; 	U -> player
plr_damage	adda spr_hp,u
		daa
		bcs 10F
		bsr plr_kill
		clra
10		sta spr_hp,u
		jmp plr_draw_hp

; Entry:
; 	U -> player
plr_kill	lda spr_state,u
		cmpa #state_plr_inactive
		bhs 99F
		lda dying
		ora plr_bit,u
		sta dying
		lda spr_state,u
		cmpa #state_plr_draw_room
		blo 10F
		cmpa #state_plr_draw_objects
		bhi 10F
99		rts
10		ldx death_fifo_end
		stu ,x++
		stx death_fifo_end
		jsr kill_speed
		lda #state_plr_die
		sta spr_state,u
		ldd obj_y,u
		; fall through to drop_key

; Drop key held by player in specified location.  Checks player has a key
; in the first place.

; Entry:
; 	A,B = y,x
; 	U -> player
drop_key
		pshs a,b,y
		lda plr_key_opens,u
		deca
		bvs 90F		; y = $80 - no key!
		jsr plr_undraw_key
		ldy #keys
10		lda obj_tile,y
		beq 20F
		leay sizeof_obj,y
		bra 10B
20		ldd plr_key_opens,u
		std obj_key_opens,y
		clra
		ldb plr_speed,u
		lslb
		adca #key
		sta obj_tile,y
		ldd ,s
		std obj_y,y
		jsr yx_to_woff
		stx obj_woff,y
		bsr set_pickup
		ldd #$8080
		std plr_key_opens,u
90		puls a,b,y,pc

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Shot processing

shot_hit_player
		ldx obj_shot_plr,u	; X -> player shot belongs to
		pshs u
		leau ,y			; U -> player being shot
		lda #$9a		; decrement hp by shot power
		suba plr_power,x
		bsr plr_damage
		puls u
		; fall through to shot_remove

shot_remove	clr obj_tile,u
		ldx obj_shot_plr,u
		dec plr_nshots,x
		bra process_next_shot

process_shots	ldu #shots
10		lda obj_tile,u
		beq process_next_shot
		ldx obj_woff,u
		bpl 12F
		leax 1,x
		beq 14F
		stx obj_woff,u
		bra process_next_shot
12		jsr spr_undraw_v
14		jsr spr_move
		bne shot_remove

		; test new shot position against drainers.
20		tfr d,x
		ldy #items
22		lda obj_tile,y
		cmpa #drainer
		bne 24F
		cmpx obj_y,y
		beq shot_hit_drainer
24		leay sizeof_obj,y
		cmpy #items_end
		blo 22B

		; test new shot position against players.  dying or dead
		; players still block shots.
test_shot_player
		ldy #players
30		cmpx obj_y,y
		beq shot_hit_player
		leay sizeof_plr,y
		cmpy #players_end
		blo 30B

		; test new shot position against monsters.  dying monsters
		; do NOT block shots, so test for this.
		ldy #monsters
40		cmpx obj_y,y
		bne 42F
		lda spr_state,y
		cmpa #state_mon_die
		blo shot_hit_monster
42		leay sizeof_mon,y
		cmpy #monsters_end
		blo 40B

		; all tests done, store new position, and calculate new
		; window offset.
		stx obj_y,u
		tfr x,d
		jsr yx_to_woff
		stx obj_woff,u

process_next_shot
		leau sizeof_obj,u
		cmpu #shots_end
		blo 10B
		rts

shot_hit_drainer
		; invert direction of shot
		lda obj_shot_facing,u
		eora #$02
		sta obj_shot_facing,u
		lda obj_tile,u
		eora #$01
		sta obj_tile,u
		; increment player's score
		pshs x,u
		ldu obj_shot_plr,u
		lda #1
		jsr plr_add_score
		puls x,u
		; decrement drainer "hp"
		dec obj_drainer_hp,y
		; if not "dead", return to check against players
		; this makes bones block reflection of shots
		bne test_shot_player
		; disable drainer in object table
		clr obj_tile,y
		; remove drainer from pickup table
		jsr check_clr_pickup
		; and return to check against players
		bra test_shot_player

shot_hit_monster
		lda #$9a
		pshs u
		ldu obj_shot_plr,u
		lda spr_hp,y
		suba plr_power,u
		bcc 10F
		lda #3
		ldx #snd_monster_die
		jsr psg_submit
		ldd #$0100|state_mon_die	; 1pt for monster
		stb spr_state,y
		jsr plr_add_score
		puls u
		jmp shot_remove
10		sta spr_hp,y
		lda #1
		ldx #snd_monster_hit
		jsr psg_submit
		puls u
		jmp shot_remove

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Pickup dispatch routines.

; Entry:
; 	A = object type
; 	Y -> object
; 	U -> player

pickup_potion
		bsr sound_low
		lda plr_score0,u
		anda #$0f
		ldb #$11
		mul
10		stb spr_hp,u
		jmp plr_draw_hp

pickup_food
		lda #3		; priority
		ldx #snd_food
		jsr psg_submit
		lda spr_hp,u
		adda #$10
		daa
		bcc 10F
		lda #$99
10		sta spr_hp,u
		jsr plr_draw_hp
		bra kill_speed

pickup_armour	bsr sound_low
		lda plr_armour,u
		inca
		cmpa #7
		bhi 99F
		sta plr_armour,u
		; fall through to plr_draw_armour

plr_draw_armour	ldx plr_swin,u
		leax 2*fb_w+3+128,x
		lda plr_armour,u
		bra draw_snum

pickup_power	bsr sound_low
		lda plr_power,u
		inca
		cmpa #9
		bhi 99F
		sta plr_power,u
		; fall through to plr_draw_power

plr_draw_power	ldx plr_swin,u
		leax 10*fb_w+2+128,x
		lda plr_power,u
		; fall through to draw_snum

draw_snum	pshs u
		ldu #snum_tiles_start-5	; no zero
		ldb #5
		mul
		leau b,u
		pulu d
		sta -128,x
		stb -96,x
		pulu d
		sta -64,x
		stb -32,x
		lda ,u
		sta ,x+
		puls u,pc

pickup_weapon
		bsr sound_low
		lda plr_ammo,u
		inca
		cmpa #3
		bhi 99F
		sta plr_ammo,u
		jmp plr_draw_ammo

sound_low
		lda #3		; priority
		ldx #snd_low
		jsr psg_submit
		; fall through to kill_speed

kill_speed	clra
		lsl plr_speed,u
		rora
		sta plr_speed,u
		; fall through to plr_undraw_speed

plr_undraw_speed
		ldx plr_swin,u
		leax 4*fb_w+2+64,x
PATCH_undraw_speed	equ *+1
		ldd #$aaaa
		sta -64,x
		stb -32,x
		sta ,x
		stb 32,x
		sta 64,x
pickup_nop
99		rts

pickup_money
		lda #3		; priority
		ldx #snd_money
		jsr psg_submit
		bra kill_speed

pickup_cross
		bsr sound_low
		lda dying
		beq 99F
		ldx death_fifo_end
		leax -2,x
		stx death_fifo_end
		ldx death_fifo
		ldd death_fifo+2
		std death_fifo
		ldd death_fifo+4
		std death_fifo+2
		ldd death_fifo+6
		std death_fifo+4
		; unset dying and dead
		lda plr_bit,x
		coma
		tfr a,b
		anda dying
		sta dying
		andb dead
		stb dead
99		rts

pickup_speed
		bsr sound_low
		; this is safe!
		inc plr_speed,u
		; fall through to plr_draw_speed

plr_draw_speed	ldx plr_swin,u
		leax 4*fb_w+2+64,x
PATCH_draw_speed0	equ *+1
		ldd #$a69a
		sta -64,x
		stb -32,x
		sta ,x
		stb 32,x
PATCH_draw_speed1	equ *+1
		lda #$6a
		sta 64,x
		rts

pickup_key
		pshs a			; preserve key type

		; key pickup preempts most other sound, as it's important
		; to know how many times player has juggled keys
		lda #4
		ldx #snd_key
		jsr psg_submit

		ldd plr_key_opens,u
		ldx obj_key_opens,y
		stx plr_key_opens,u
		std obj_key_opens,y

		clra
		ldb plr_speed,u		; top bit of plr_speed is key type
		lslb
		adca #key		; tile is #key or #key+1
		sta obj_tile,y		; update object tile

		puls a			; which key type was picked up?
		lsra			; tile id was shifted left
		lsra			; and now we want lowest bit
		rorb			; into top bit of plr_speed
		stb plr_speed,u

		; clear object tile if player wasn't actually holding a
		; key.  means some of above was redundant...
		ldd obj_key_opens,y
		cmpd #$8080
		bne 10F
		clr obj_tile,y
		ldx obj_y,u
		bsr check_clr_pickup
10		; fall through to plr_draw_key

plr_draw_key	ldx plr_swin,u
		leax 2+32,x
		lda plr_speed,u
		bmi plr_draw_key1
PATCH_draw_key0	equ *+1
		ldd #$a656
		sta -32,x
		stb ,x
PATCH_draw_key1	equ *+1
		lda #$66
		sta 32,x
		rts

plr_draw_key1
PATCH_draw_key01	equ *+1
		ldd #$a202
		sta -32,x
		stb ,x
PATCH_draw_key11	equ *+1
		lda #$22
		sta 32,x
99		rts

; Clear bitmap entry for object y.
; Entry:
; 	Y -> object
obj_clr_pickup	pshs a,x,y
		ldd obj_y,y
		bra clr_pickup_d_

; Clear bitmap entry for y,x (held in x) if no other pickups there.  Only
; necessary to check like this for keys and drainers.
; Entry:
; 	X = y,x
check_clr_pickup
		pshs a,x,y
		ldy #pickups
10		lda obj_tile,y
		beq 20F
		cmpx obj_y,y
		beq 30F
20		leay sizeof_obj,y
		cmpy #pickups_end
		blo 10B
clr_pickup_x_	tfr x,d
clr_pickup_d_	ldx #tbl_x_to_bit
		ldb b,x
		ldx #bmp_objects+128
		comb
		andb a,x
		stb a,x
30		puls a,x,y,pc

; Clear bitmap entry for y,x (held in x) without checking.
; Entry:
; 	X = y,x
clr_pickup_x	pshs a,x,y
		bra clr_pickup_x_

		DATA
tbl_x_to_bit	fcb $80,$40,$20,$10
		fcb $08,$04,$02,$01
		CODE

pickup_exit
		lda exiting
		bne 99B
		lda #facing_up
		jsr spr_move_a
		beq 10F
		lda #facing_right
		jsr spr_move_a
		beq 10F
		lda #facing_down
		jsr spr_move_a
		beq 10F
		lda #facing_left
		jsr spr_move_a
		; XXX assuming one will work
10		jsr drop_key
		lda #state_plr_exit0
		sta spr_state,u
		sta exiting
		; invalidate x, but store its value in cx for use drawing
		; exit sprites
		lda obj_x,u
		sta spr_cx,u
		com obj_x,u
		ldd #tbl_exit_data
		std spr_tmp0,u
		jsr kill_speed
		lda #4		; priority
		ldx #snd_exit
		jsr psg_submit
		; skip return address, stacked object type and return:
		bra 90F

		; not a pickup: teleport player
pickup_tport
		lda #4		; priority
		ldx #snd_tport
		jsr psg_submit
10		leay sizeof_obj,y
		cmpy #items_end
		blo 20F
		ldy #items
20		lda obj_tile,y
		cmpa #tport
		bne 10B
		ldd obj_y,y
		deca
		std spr_next_y,u
		anda #$f8
		cmpa plr_yroom,u
		beq 30F
		dec plr_yroom,u	; invalidate yroom
		lda #state_plr_draw_room
		bra 40F
30		lda #state_plr_landed
40		sta spr_state,u
89		jsr kill_speed	; XXX test this
90		leas 3,s	; skip return address, stacked object type
		puls x,y,pc

		; not a pickup: drain health
pickup_drainer
		lda #3		; priority
		ldx #snd_drainer
		jsr psg_submit
mod_drainer_dmg	equ *+1
		lda #$80	; -20HP
		jsr plr_damage
		; kill speed, skip return address, stacked object type, return
		bra 89B

plr_exit1
		lda #state_plr_exitdelay-2
		sta spr_state,u
		; fall through to plr_exit0

plr_exit0
		ldx spr_tmp0,u
		cmpx #tbl_exit_data_end
		bhs 10F
		lda ,x+
		adda spr_tilebase,u
		sta obj_tile,u
		stx spr_tmp0,u
		jsr spr_draw_cx
		jmp spr_inc_state

10
		clr exiting
		lda finished
		ora plr_bit,u
		sta finished
		lda #state_plr_inactive
		sta spr_state,u
		lda #exit
		sta obj_tile,u
		jmp spr_draw_cx

		DATA

tbl_exit_data	fcb p1_up0-p1_tilebase
		fcb p1_right0-p1_tilebase
		fcb p1_down0-p1_tilebase
		fcb p1_left0-p1_tilebase
		fcb p1_exit4-p1_tilebase
		fcb p1_exit5-p1_tilebase
		fcb p1_exit6-p1_tilebase
		fcb p1_exit7-p1_tilebase
		fcb p1_exit8-p1_tilebase
		fcb p1_exit9-p1_tilebase
		fcb p1_exit10-p1_tilebase
		fcb p1_exit11-p1_tilebase
tbl_exit_data_end

tbl_pickup_scores	equ *-1		; floor not a pickup
		fcb 0		; exit
		fcb 0		; trapdoor
		fcb $05		; food
		fcb 0		; tport
		fcb $10		; armour
		fcb $10		; potion
		fcb $10		; weapon
		fcb $10		; cross
		fcb $10		; speed
		fcb 0		; drainer
		fcb $20		; money
		fcb $10		; power
		fcb 0		; dummy
		fcb 0		; key
		fcb 0		; key1

jtbl_pickup	equ *-2			; no dispatch for floor
		fdb pickup_exit
		fdb pickup_nop		; trapdoor
		fdb pickup_food
		fdb pickup_tport
		fdb pickup_armour
		fdb pickup_potion
		fdb pickup_weapon
		fdb pickup_cross
		fdb pickup_speed
		fdb pickup_drainer
		fdb pickup_money
		fdb pickup_power
		fdb pickup_key		; dummy
		fdb pickup_key
		fdb pickup_key		; key1

		CODE

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Wait for any key

wait_keypress
		; wait if key held
10		bsr firebutton_pressed
		bne 10B
		bsr wait_keyheld
		; wait until key pressed
10		lda reg_pia0_pdra
		ora #$80
		inca
		beq 10B
		bsr firebutton_pressed
		bne 10B
		rts

wait_keyheld
		; wait if key held
10		bsr test_keypressed
		bne 10B
		rts

test_keypressed
		clr reg_pia0_pdrb
		lda reg_pia0_pdra
		ora #$80
		inca
		rts

; For menus: scan table of KEYDSP_ENTRY and jump to handler

wait_keypress_jump
		bsr wait_keyheld
10		bsr test_keypress_jump
		bra 10B


test_keypress_jump
		; test firebutton before & after scan
20		bsr firebutton_pressed
		bne 99F
		leau -2,x
30		leau 4,u
		ldd -2,u
		beq 99F
		sta reg_pia0_pdrb
		bitb reg_pia0_pdra
		bne 30B
		bsr firebutton_pressed
		bne 99F
		leas 2,s
		pulu pc
99		rts

firebutton_pressed
		lda #$ff
		sta reg_pia0_pdrb
		lda reg_pia0_pdra
		ora #$80
		inca
99		rts

; Entry:
; 	U -> textlist
; textlist:
; 	[ y, x, &string ]..., 0

write_textlist
10		lda ,u+
		beq 99B
		deca
		pulu b,y
		bsr write_at
		bra 10B

; Entry:
; 	U -> charlist
; charlist:
; 	[ y, x, char ] ..., 0

write_charlist
10		lda ,u+
		beq 99B
		deca
		pulu b
		std text_cursor_y
		pulu a
		bsr write_char
		bra 10B

; Entry:
; 	Y -> y, x, string (write_screen)
; 	Y -> string (write_at)
; 	A, B = y, x (write_at)
; string:
; 	[ char ]..., 0

write_screen	ldd ,y++		; y,x
write_at	std text_cursor_y
		stb text_line_x
10		lda ,y+
		beq 99B
		cmpa #$7d	; Dunjunz logo
		beq write_logo
		cmpa #$7e	; Nudge left (kerning)
		beq 50F
		cmpa #$7f	; LF
		beq 40F
		bsr write_char
		bra 10B

write_logo	bsr get_write_x
		pshs u
		ldu #logo
		lda #17
		pshs a
30		lda #15
32		ldb ,u+
		stb ,x+
		deca
		bne 32B
		leax fb_w-15,x
		dec ,s
		bne 30B
		puls a,u
		; fall through to LF

		; LF
40		ldd text_line_x
		sta text_cursor_x
mod_line_height	equ *+1
		addb #11
		stb text_cursor_y
		bra 10B

		; Nudge left (kerning)
50		dec text_cursor_x
		bra 10B

get_write_x	lda text_cursor_y
		ldb #32
		mul
		addd #fb0
		tfr d,x
		ldb text_cursor_x
		lsrb
		lsrb
		abx
		rts

write_char	pshs u
		sta text_descender	; top bit set indicates descender
		anda #$7f
		ldb #7
		mul
		addd #textfont
		tfr d,u
		pulu a
		sta text_char_width
		bsr get_write_x

		lda #6
		sta tmp2+1
10		lda ,u+
		bsr text_emit_byte
		dec tmp2+1
		bne 10B

		lda text_descender
		bpl text_char_next
		anda #$06
		ldu #jtbl_descender
		jmp [a,u]

text_emit_byte	bsr text_draw_byte
		leax fb_w,x
		rts
text_draw_byte	ldb text_cursor_x
		pshs b
		ldb #$ff
		lsr ,s
		bcc 10F
		rora
		rorb
		rora
		rorb
10		lsr ,s+
		bcc 20F
		rora
		rorb
		rora
		rorb
		rora
		rorb
		rora
		rorb
20		std text_tmp0
		coma
		comb
		anda text_colour
		andb text_colour
		std text_tmp1
		ldd ,x
		anda text_tmp0
		andb text_tmp0+1
		addd text_tmp1
		std ,x
		rts

descender_gy
		lda #~$0c
		bsr text_emit_byte
		lda #~$30
		bsr text_emit_byte
		lda #~$c0
		bsr text_draw_byte
		leax -1,x
		lda #~$ff
		bsr text_draw_byte
		leax -1,x
10		lda #~$03
20		bsr text_draw_byte
		; fall through to text_char_next

text_char_next
		lda text_cursor_x
		adda text_char_width
		sta text_cursor_x
		puls u,pc

descender_comma
		lda #~$c0
		bsr text_draw_byte
		leax fb_w-1,x
		bra 10B

descender_p
		lda #~$c0
		bsr text_emit_byte
		lda #~$c0
		bsr text_emit_byte
		lda #~$c0
		bra 20B

descender_q
		lda #~$0c
		bsr text_emit_byte
		lda #~$0c
		bsr text_emit_byte
		lda #~$0c
		bra 20B

		DATA
jtbl_descender	fdb descender_gy
		fdb descender_comma
		fdb descender_p
		fdb descender_q
		CODE

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Switch to the next video mode and apply relevant palette switching and
; code patchset.  Video modes are in order: PAL, NTSC 1, NTSC 2.

palette_swap_a	ldb #9
10		bsr palette_swap_u
		bsr palette_swap_u
		anda #$f0
		sta -1,u
		decb
		bne 10B
		rts

palette_swap_b	ldb #9
10		bsr palette_swap_u
		anda #$0f
		sta -1,u
		bsr palette_swap_u
		decb
		bne 10B
		rts

; Simple palette shifter - optimised version delicately lifted from
; Stewart Orchard's blog.

palette_swap_u	pshs b
		lda ,u
		ldb #4
		stb tmp0
10		clrb
		lsla
		rolb
		lsla
		rolb
		ora b,x
		dec tmp0
		bne 10B
		sta ,u+
		puls b,pc

advance_video_mode
		lda video_mode
		inca
		cmpa #3
		blo 10F
		clra
10		sta video_mode
		ldb #10
		mul
		ldx #video_mode_transitions
		leax d,x

		ldu #tiles_a_start
20		bsr palette_swap_a
		cmpu #tiles_a_end
		blo 20B

30		bsr palette_swap_a	; chalice_nw0
		bsr palette_swap_b	; chalice_ne0
		cmpu #chalice_end
		blo 30B

		bsr palette_swap_b	; yes
		bsr palette_swap_b	; no

		ldu #tiles_b_start
40		bsr palette_swap_b
		cmpu #tiles_b_end
		blo 40B

		; Palette-swap large numbers, logo
		ldu #lnum_tiles_start
50		bsr palette_swap_u
		cmpu #logo_end
		blo 50B

		leax 4,x		; Palette-swap small numbers
		;ldu #snum_tiles_start	; ASSUMED == lnum_tiles_end
60		bsr palette_swap_u
		cmpu #snum_tiles_end
		blo 60B

		ldu 4,x			; Apply NTSC patches
		; fall through to apply_patchset

; Entry:
; 	U -> patchset
; See top of file for description of patchset.

apply_patchset	pulu b,x	; B = count
		clra
10		leax a,x	; skip A bytes
20		pulu a
		sta ,x+
		decb
		bne 20B
		ldb ,u+		; B = 16-bit skip flag | count
		beq 99F		; 0 indicates finished
		bmi 30F		; bit 7 set indicates 16-bit skip
		pulu a		; A = 8-bit skip
		bra 10B
30		andb #$7f	; clear 16-bit skip flag
		pshs b
		pulu d
		leax d,x
		puls b		; B = count
		bra 20B
99		rts

		DATA

video_mode_transitions
		fcb 2,3,0,1			; ntsc1 -> pal palette0
		fcb 0,3,2,1			; ntsc1 -> pal palette1
		fdb ntsc1_pal_patchset		; ntsc1 -> pal patchset
		fcb 1,3,0,2			; pal -> ntsc0 palette0
		fcb 0,3,1,2			; pal -> ntsc0 palette1
		fdb pal_ntsc0_patchset		; pal -> ntsc0 patchset
		fcb 0,2,1,3			; ntsc0 -> ntsc1 palette0
		fcb 0,2,1,3			; ntsc0 -> ntsc1 palette1
		fdb ntsc0_ntsc1_patchset	; ntsc0 -> ntsc1 patchset

; PAL -> NTSC phase0 patchset

pal_ntsc0_patchset

		PATCH colour_bg
		fcb $00,$55,$ff,$aa
		ENDPATCH

		PATCH PATCH_vdgmode_unpack_screen
		fcb $fa		; lda #$fa - VDG mode RG6, CSS1
		ENDPATCH

		PATCH PATCH_draw_ammo0
		fcb $ff,$20	; ldd #$ff20
		ENDPATCH
		PATCH PATCH_draw_ammo1
		fcb $e8		; ldb #$e8
		ENDPATCH
		PATCH PATCH_draw_ammo2
		fcb $20		; ldb #$20
		ENDPATCH
		PATCH PATCH_draw_ammo3
		fcb $00,$00	; ldd #$0000
		ENDPATCH
		PATCH PATCH_draw_key0
		fcb $0c,$fc	; ldd #$0cfc
		ENDPATCH
		PATCH PATCH_draw_key1
		fcb $cc		; lda #$cc
		ENDPATCH
		PATCH PATCH_draw_key01
		fcb $04,$54	; ldd #$0454
		ENDPATCH
		PATCH PATCH_draw_key11
		fcb $44		; lda #$44
		ENDPATCH
		PATCH PATCH_undraw_key
		fcb $00,$00	; ldd #$0000
		ENDPATCH
		PATCH PATCH_draw_speed0
		fcb $0c,$30	; ldd #$0c30
		ENDPATCH
		PATCH PATCH_draw_speed1
		fcb $c0		; lda #$c0
		ENDPATCH
		PATCH PATCH_undraw_speed
		fcb $00,$00	; ldd #$0000
		ENDPATCH

		; directly patch play screen dz.

		PATCH play_screen_dz+1
		fcb $00
		ENDPATCH
		PATCH play_screen_dz+6
		fcb $03,$ff
		ENDPATCH
		PATCH play_screen_dz+11
		fcb $c3
		ENDPATCH
		PATCH play_screen_dz+15
		fcb $c0
		ENDPATCH
		PATCH play_screen_dz+21
		fcb $c3
		ENDPATCH
		PATCH play_screen_dz+27
		fcb $ff
		ENDPATCH
		PATCH play_screen_dz+36
		fcb $c3
		ENDPATCH
		PATCH play_screen_dz+40
		fcb $00,$3c
		ENDPATCH
		PATCH play_screen_dz+48
		fcb $00
		ENDPATCH

		; same for text screen.

		PATCH text_screen_dz+1
		fcb $00
		ENDPATCH
		PATCH text_screen_dz+6
		fcb $03,$ff
		ENDPATCH
		PATCH text_screen_dz+11
		fcb $c0
		ENDPATCH
		PATCH text_screen_dz+19
		fcb $ff
		ENDPATCH
		PATCH text_screen_dz+28
		fcb $c3
		ENDPATCH
		PATCH text_screen_dz+32
		fcb $00,$3c
		ENDPATCH
		PATCH text_screen_dz+40
		fcb $00
		ENDPATCH

		ENDPATCHSET

; NTSC phase0 -> NTSC phase1 patchset

ntsc0_ntsc1_patchset

		PATCH colour_bg
		fcb $00,$aa,$ff,$55
		ENDPATCH

		PATCH PATCH_draw_ammo0+1
		fcb $10	; ldd #$ff10
		ENDPATCH
		PATCH PATCH_draw_ammo1
		fcb $d4		; ldb #$d4
		ENDPATCH
		PATCH PATCH_draw_ammo2
		fcb $10		; ldb #$10
		ENDPATCH
		PATCH PATCH_draw_key01
		fcb $08,$a8	; ldd #$08a8
		ENDPATCH
		PATCH PATCH_draw_key11
		fcb $88		; lda #$88
		ENDPATCH

		ENDPATCHSET

; NTSC phase1 -> PAL patchset

ntsc1_pal_patchset

		PATCH colour_bg
		fcb $aa,$00,$55,$ff
		ENDPATCH

		PATCH PATCH_vdgmode_unpack_screen
		fcb $e2		; lda #$e2 - VDG mode CG6, CSS0
		ENDPATCH

		PATCH PATCH_draw_ammo0
		fdb $55ba	; ldd #$55ba
		ENDPATCH
		PATCH PATCH_draw_ammo1
		fcb $7e		; ldb #$7e
		ENDPATCH
		PATCH PATCH_draw_ammo2
		fcb $ba		; ldb #$ba
		ENDPATCH
		PATCH PATCH_draw_ammo3
		fcb $aa,$aa	; ldd #$aaaa
		ENDPATCH
		PATCH PATCH_draw_key0
		fdb $a656	; ldd #$a656
		ENDPATCH
		PATCH PATCH_draw_key1
		fcb $66		; lda #$66
		ENDPATCH
		PATCH PATCH_draw_key01
		fdb $a202	; ldd #$a202
		ENDPATCH
		PATCH PATCH_draw_key11
		fcb $22		; lda #$22
		ENDPATCH
		PATCH PATCH_undraw_key
		fcb $aa,$aa	; ldd #$aaaa
		ENDPATCH
		PATCH PATCH_draw_speed0
		fdb $a69a	; ldd #$a69a
		ENDPATCH
		PATCH PATCH_draw_speed1
		fcb $6a		; lda #$6a
		ENDPATCH
		PATCH PATCH_undraw_speed
		fcb $aa,$aa	; ldd #$aaaa
		ENDPATCH

		; directly patch play screen dz.

		PATCH play_screen_dz+1
		fcb $aa
		ENDPATCH
		PATCH play_screen_dz+6
		fcb $a9,$55
		ENDPATCH
		PATCH play_screen_dz+11
		fcb $69
		ENDPATCH
		PATCH play_screen_dz+15
		fcb $6a
		ENDPATCH
		PATCH play_screen_dz+21
		fcb $69
		ENDPATCH
		PATCH play_screen_dz+27
		fcb $55
		ENDPATCH
		PATCH play_screen_dz+36
		fcb $41
		ENDPATCH
		PATCH play_screen_dz+40
		fcb $82,$96
		ENDPATCH
		PATCH play_screen_dz+48
		fcb $82
		ENDPATCH

		; same for text screen.

		PATCH text_screen_dz+1
		fcb $aa
		ENDPATCH
		PATCH text_screen_dz+6
		fcb $a9,$55
		ENDPATCH
		PATCH text_screen_dz+11
		fcb $6a
		ENDPATCH
		PATCH text_screen_dz+19
		fcb $55
		ENDPATCH
		PATCH text_screen_dz+28
		fcb $41
		ENDPATCH
		PATCH text_screen_dz+32
		fcb $82,$96
		ENDPATCH
		PATCH text_screen_dz+40
		fcb $82
		ENDPATCH

		ENDPATCHSET

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		DATA

		include "tiles.s"
		include "fonts.s"

tbl_tiles_a_start

		fdb tile_door1_v_a
		fdb tile_door1_h_a
		fdb tile_door_v_a
		fdb tile_door_h_a

tbl_tiles_a

		fdb tile_floor_a
		fdb tile_exit_a
		fdb tile_trapdoor_a
		fdb tile_food_a
		fdb tile_tport_a
		fdb tile_armour_a
		fdb tile_potion_a
		fdb tile_weapon_a
		fdb tile_cross_a
		fdb tile_speed_a
mod_drainer_a	fdb tile_drainer0_a
mod_money_a	fdb tile_money0_a
mod_power_a	fdb tile_power0_a
		fdb tile_key_a	; dummy
		fdb tile_key_a
		fdb tile_key1_a

		fdb tile_p1_up0_a
		fdb tile_p1_up1_a
		fdb tile_p1_down0_a
		fdb tile_p1_down1_a
		fdb tile_p1_left0_a
		fdb tile_p1_left1_a
		fdb tile_p1_right0_a
		fdb tile_p1_right1_a
		fdb tile_arrow_up_a
		fdb tile_arrow_down_a
		fdb tile_arrow_left_a
		fdb tile_arrow_right_a
		fdb tile_p1_exit4_a
		fdb tile_p1_exit5_a
		fdb tile_p1_exit6_a
		fdb tile_p1_exit7_a
		fdb tile_exit8_a
		fdb tile_exit9_a
		fdb tile_exit10_a
		fdb tile_exit11_a

		fdb tile_p2_up0_a
		fdb tile_p2_up1_a
		fdb tile_p2_down0_a
		fdb tile_p2_down1_a
		fdb tile_p2_left0_a
		fdb tile_p2_left1_a
		fdb tile_p2_right0_a
		fdb tile_p2_right1_a
		fdb tile_fball_up_a
		fdb tile_fball_down_a
		fdb tile_fball_left_a
		fdb tile_fball_right_a
		fdb tile_p2_exit4_a
		fdb tile_p2_exit5_a
		fdb tile_p2_exit6_a
		fdb tile_p2_exit7_a
		fdb tile_exit8_a
		fdb tile_exit9_a
		fdb tile_exit10_a
		fdb tile_exit11_a

		fdb tile_p3_up0_a
		fdb tile_p3_up1_a
		fdb tile_p3_down0_a
		fdb tile_p3_down1_a
		fdb tile_p3_left0_a
		fdb tile_p3_left1_a
		fdb tile_p3_right0_a
		fdb tile_p3_right1_a
		fdb tile_axe_up_a
		fdb tile_axe_down_a
		fdb tile_axe_left_a
		fdb tile_axe_right_a
		fdb tile_p3_exit4_a
		fdb tile_p3_exit5_a
		fdb tile_p3_exit6_a
		fdb tile_p3_exit7_a
		fdb tile_exit8_a
		fdb tile_exit9_a
		fdb tile_exit10_a
		fdb tile_exit11_a

		fdb tile_p4_up0_a
		fdb tile_p4_up1_a
		fdb tile_p4_down0_a
		fdb tile_p4_down1_a
		fdb tile_p4_left0_a
		fdb tile_p4_left1_a
		fdb tile_p4_right0_a
		fdb tile_p4_right1_a
		fdb tile_sword_up_a
		fdb tile_sword_down_a
		fdb tile_sword_left_a
		fdb tile_sword_right_a
		fdb tile_p4_exit4_a
		fdb tile_p4_exit5_a
		fdb tile_p4_exit6_a
		fdb tile_p4_exit7_a
		fdb tile_exit8_a
		fdb tile_exit9_a
		fdb tile_exit10_a
		fdb tile_exit11_a

		fdb tile_monster_up0_a
		fdb tile_monster_up1_a
		fdb tile_monster_down0_a
		fdb tile_monster_down1_a
		fdb tile_monster_left0_a
		fdb tile_monster_left1_a
		fdb tile_monster_right0_a
		fdb tile_monster_right1_a

		fdb tile_bones0_a
		fdb tile_bones1_a
		fdb tile_bones2_a
		fdb tile_bones3_a
		fdb tile_bones4_a
		fdb tile_bones5_a
		fdb tile_bones6_a
		fdb tile_bones7_a

tbl_tiles_a_end

tbl_lnum	fdb tile_lnum_0,tile_lnum_1
		fdb tile_lnum_2,tile_lnum_3
		fdb tile_lnum_4,tile_lnum_5
		fdb tile_lnum_6,tile_lnum_7
		fdb tile_lnum_8,tile_lnum_9

		; Zeroed areas to be populated by tile shifting code in
		; INIT.  Try to keep these together for better
		; compression.

tiles_b_start
tile_drainer0_b	equ *+tile_drainer0_a-tiles_a_start
tile_drainer1_b	equ *+tile_drainer1_a-tiles_a_start
tile_money0_b	equ *+tile_money0_a-tiles_a_start
tile_money1_b	equ *+tile_money1_a-tiles_a_start
tile_power0_b	equ *+tile_power0_a-tiles_a_start
tile_power1_b	equ *+tile_power1_a-tiles_a_start
tile_floor_b	equ *+tile_floor_a-tiles_a_start
		rzb tiles_a_end-tiles_a_start
tiles_b_end

tbl_tiles_b_start
tbl_tiles_b	equ *+tbl_tiles_a-tbl_tiles_a_start
mod_drainer_b	equ *+mod_drainer_a-tbl_tiles_a_start
mod_money_b	equ *+mod_money_a-tbl_tiles_a_start
mod_power_b	equ *+mod_power_a-tbl_tiles_a_start
		rzb tbl_tiles_a_end-tbl_tiles_a_start

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		DATA

		include "text.s"

levels
level1		includebin "level01.bin"
level2		includebin "level02.bin"
level3		includebin "level03.bin"
level4		includebin "level04.bin"
level5		includebin "level05.bin"
level6		includebin "level06.bin"
level7		includebin "level07.bin"
level8		includebin "level08.bin"
level9		includebin "level09.bin"
level10		includebin "level10.bin"
level11		includebin "level11.bin"
level12		includebin "level12.bin"
level13		includebin "level13.bin"
level14		includebin "level14.bin"
level15		includebin "level15.bin"
level16		includebin "level16.bin"
level17		includebin "level17.bin"
level18		includebin "level18.bin"
level19		includebin "level19.bin"
level20		includebin "level20.bin"
level21		includebin "level21.bin"
level22		includebin "level22.bin"
level23		includebin "level23.bin"
level24		includebin "level24.bin"
	if DEBUG_LEVEL25
level25		includebin "level25d.bin"
	else
level25		includebin "level25.bin"
	endif

; free space within level25:
;
;   32 bytes from +0x10
;   32 bytes from +0x40
;   32 bytes from +0x70
;   40 bytes from +0x98
;
; except i've nothing left small enough to put there!

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		BSS

monsters	equ *+mon_offset_
		rmb max_monsters*sizeof_mon
monsters_end	equ *+mon_offset_

; Wall bitmap.  Bits set where wall or door exists.  Tested against before
; moving, and door bits cleared when opened.

bmp_wall	rmb sizeof_bmp
bmp_wall_end

; Objects bitmap.  Bits set where any object lies.  Used to determine if
; pickups need processed, or if a non-floor tile needs redrawn when a
; sprite moves.

bmp_objects	rmb sizeof_bmp
bmp_objects_end

objects
doors		rmb max_doors*sizeof_obj
doors_end
pickups
keys		rmb max_keys*sizeof_obj
keys_end
items		rmb max_items*sizeof_obj
items_end
pickups_end
trapdoors	rmb max_trapdoors*sizeof_obj
trapdoors_end
objects_end
shots		rmb 12*sizeof_obj
shots_end
all_objects_end

; ========================================================================

; This code is run once on startup, then discarded.  It's positioned at
; the beginning of the uninitialised data area (BSS).

; Note this shouldn't be used to initialise anything in BSS, obviously!
; That should happen in the main CODE section.

		section "INIT"

		org BSS_start

		export INIT_exec
INIT_exec
		setdp 0			; assumed

		orcc #$50

		; patch reset vector
		ldd #init_reset
		std $0072
		lda #$55
		sta $0071

init_reset
		nop
		orcc #$50
		lds #$0100

		lda #$ff
		tfr a,dp
		setdp $ff

		ldx #fb0
		ldd #$aaaa
10		std ,x++
		cmpx #fb0_end
		blo 10B

		ldd #$343c
		sta reg_pia0_cra	; HS disabled
		sta reg_pia1_cra	; printer FIRQ disabled
		stb reg_pia1_crb	; CART FIRQ disabled
		inca
		sta reg_pia0_crb	; FS enabled hi->lo

		; SAM MPU rate = slow
		sta reg_sam_r1c
		sta reg_sam_r0c

		; SAM display offset = $0200
		sta reg_sam_f0s
		sta reg_sam_f1c
		sta reg_sam_f2c

		; SAM VDG mode = G6R,G6C
		sta reg_sam_v0c
		sta reg_sam_v1s
		sta reg_sam_v2s

		lda #gamedp
		tfr a,dp
		setdp gamedp

		lda $a000		; [$A000] points to ROM1 in CoCos
		anda #$20
		beq 10F			; Dragon - no patching necessary
		ldu #coco_patchset	; Apply CoCo patches
		jsr apply_patchset
10

		; test for 64K
		sta reg_sam_tys
		lda $0062
		ldb $8063
		coma		; a != [$0062]
		comb		; b != [$0063]
		std $8062
		cmpd $8062
		bne no_64k	; didn't write
		cmpd $0062
		beq no_64k	; *did* shadow write
		ldu #m64k_patchset
		jsr apply_patchset
		bra 10F
no_64k		sta reg_sam_tyc
10

		; generate shifted tiles
		ldu #tbl_tiles_a_start
		ldx #tbl_tiles_b_start
10		pulu d
		addd #tiles_b_start-tiles_a_start
		std ,x++
		cmpu #tbl_tiles_a_end
		blo 10B
		ldu #tiles_a_start
		ldx #tiles_b_start
20		pulu d
		lsra
		rorb
		lsra
		rorb
		lsra
		rorb
		lsra
		rorb
		std ,x++
		cmpu #tiles_a_end
		blo 20B

		ldd #$aa00
		std colour_bg
		ldd #$55ff
		std colour_hi

		; NTSC detection
		clr video_mode
		ldx #0
		lda reg_pia0_pdrb	; clear outstanding IRQ
		sync			; wait for FS hi->lo
		lda reg_pia0_pdrb	; clear outstanding IRQ
10		lda reg_pia0_crb	; 5 - test for IRQ
		bmi 20F			; 3
		mul			; 11 - waste time...
		mul			; 11
		mul			; 11
		leax ,x			; 4
		leax ,x			; 4
		leax 1,x		; 5 - inc line count
		bra 10B			; 3 = 57 (cycles per scanline)
20		cmpx #288		; PAL = 312, NTSC = 262
		bhs 30F
		jsr advance_video_mode
30

		; patch reset vector
		ldd #game_reset
		std $0072
		ldd #$55ff
		sta $0071
		; set level = -1
		stb level

		clr difficulty
		;lda #$0f
		clr playing

		lda #$ff
		sta dead
		jsr init_players

		jmp game_reset

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; 64K patches

m64k_patchset

		PATCH mod_show_title
		fcb $21			; brn
		ENDPATCH

		PATCH PATCH_pause
		fdb play_song
		ENDPATCH

		ENDPATCHSET

		; - - - - - - - - - - - - - - - - - - - - - - -

; CoCo patches

coco_patchset

		PATCH tbl_kdsp_menu
		KEYDSP_ENTRY 7,3,start_game	; SPACE
		KEYDSP_ENTRY 1,4,toggle_p1	; '1'
		KEYDSP_ENTRY 2,4,toggle_p2	; '2'
		KEYDSP_ENTRY 3,4,toggle_p3	; '3'
		KEYDSP_ENTRY 4,4,toggle_p4	; '4'
		KEYDSP_ENTRY 4,0,toggle_skill	; 'D'
		KEYDSP_ENTRY 6,2,toggle_vmode	; 'V'
		ENDPATCH

		PATCH tbl_p1_keys
		KEYTBL_ENTRY 1,4,2			; '1' = magic
		KEYTBL_ENTRY 2,4,$80|facing_up		; '2' = up
		KEYTBL_ENTRY 7,2,$80|facing_down	; 'W' = down
		KEYTBL_ENTRY 4,4,$80|facing_right	; '4' = right
		KEYTBL_ENTRY 3,4,$80|facing_left	; '3' = left
		KEYTBL_ENTRY 5,4,1			; '5' = fire
		fdb 0
		KEYTBL_ENTRY 7,5,2			; '/' = magic
		KEYTBL_ENTRY 0,2,$80|facing_up		; 'P' = up
		KEYTBL_ENTRY 3,5,$80|facing_down	; ';' = down
		KEYTBL_ENTRY 6,5,$80|facing_right	; '.' = right
		KEYTBL_ENTRY 4,5,$80|facing_left	; ',' = left
		KEYTBL_ENTRY 0,4,1			; '0' = fire
		fdb 0
		ENDPATCH

		PATCH PATCH_text_p1_controls_ptr+2
		fdb text_p1_controls_coco
		ENDPATCH
		PATCH PATCH_text_p2_controls_ptr+2
		fdb text_p2_controls_coco
		ENDPATCH

		PATCH PATCH_p1ddrb
		fcb $f8		; lda #$f8 - VDG ONLY as outputs
		ENDPATCH

		ENDPATCHSET

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Useful addresses for end of listing

		export CODE_start
		export CODE_end
		export DATA_start
		export DATA_end
		export BSS_start
		export BSS_end
		export INIT_end

CODE_start	equ CODE_start_
CODE_end	CODE
DATA_start	equ DATA_start_
DATA_end	DATA
BSS_start	equ BSS_start_
BSS_end		BSS
INIT_end	section "INIT"
