#!/usr/bin/perl -wT
use strict;

binmode STDIN;

my @bytes = ();
my $text;
while (read(STDIN, $text, 1) == 1) {
	my $v = ord($text) & 0xff;
	push @bytes, sprintf("\$\%02x", $v);
	if (scalar(@bytes) == 8) {
		print "\t\tfcb ".join(",",@bytes)."\n";
		@bytes = ();
	}
}
print "\t\tfcb ".join(",",@bytes)."\n" if (scalar(@bytes) != 0);
