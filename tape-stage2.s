; Dunjunz - remade for the Dragon 32/64
; Copyright (c) 2017-2018 Ciaran Anscomb
;
; See COPYING file for redistribution conditions.

; Tape second stage loader.  bin2cas.pl automatically generatates the
; first stage, but we need more functionality than bin2cas.pl supports.
;
; Wraps up title screen and copyright message.  Dzipped title screen is
; copied into high RAM for later use if 64K is detected, otherwise
; discarded.

; loader function pointers
load_part_ptr	equ $01e9
dunzip_ptr	equ $01eb

TAPE		equ 1

		include "dragonhw.s"
		include "dunjunz.exp"
		include "tape-part1.exp"

		org $0300
		setdp 0		; assumed

start

		orcc #$50
		lds #$0300

		; visible loading indicator
		lda #$b8
		sta $0400

		; use different pages for $c000-$fdff on coco3
		ldd #$3637
		std $ffa6

		; load part1
		ldx #PART1_start
		jsr [load_part_ptr]

		; test for 64K
		sta reg_sam_tys
		lda $0062
		ldb $8063
		coma		; a != [$0062]
		comb		; b != [$8063]
		std $8062
		cmpd $8062
		bne no_64k	; didn't write
		cmpd $0062
		beq no_64k	; *did* shadow write
		inc mod_is_64k

		; copy low ram vars to $8000+
		ldx #$0000
		ldu #$8000
10		ldd ,x++
		std ,u++
		cmpx #$0500
		blo 10B

		; copy title & copyright64 dz to $8500+
		ldx #PART1_title_dz
		;ldu #$8500
10		lda ,x+
		sta ,u+
		cmpx #PART1_copyright64_dz_end
		blo 10B

no_64k

		sta reg_sam_tyc
		ldx #$0400
		ldd #$aaaa
10		std ,x++
		cmpx #$1c00
		blo 10B

		sta reg_sam_v0c
		sta reg_sam_v1s
		sta reg_sam_v2s
		lda #$e0
		sta reg_pia1_pdrb

		; dunzip title
		ldx #PART1_title_dz
		ldd #PART1_title_dz_end
		ldu #$0400+6*32
		jsr [dunzip_ptr]

		; dunzip copyright
		ldx #PART1_copyright_dz
		ldd #PART1_copyright_dz_end
		ldu #$0400+171*32
		jsr [dunzip_ptr]

		; load game dz (part2)
		ldx #$3700
		jsr [load_part_ptr]
		pshs x

mod_is_64k	bra 20F		; incremented to brn if 64K

		; tidy up where flasher was
		lda #$aa
		sta $0400
		; dunzip copyright64
		sta reg_sam_tys
		ldx #$8500+PART1_copyright64_dz-PART1_start
		ldd #$8500+PART1_copyright64_dz_end-PART1_start
		ldu #$0400+171*32
		jsr [dunzip_ptr]
		sta reg_sam_tyc

		; load music dz (part3) in page#1
		sta reg_sam_p1s
		ldd #$3637
		std $ffa2
		ldx #$4000		; $c000, mapped lower
		jsr [load_part_ptr]
		sta reg_sam_p1c
		ldd #$3a3b
		std $ffa2
		sta reg_sam_tys

		; dunzip music dz to $9500+
		tfr x,d
		ora #$80
		ldx #$c000
		ldu #$9500
		jsr [dunzip_ptr]
		sta reg_sam_tyc

20		; clear screen neatly
		lda reg_pia0_pdrb	; clear outstanding IRQ
		sync
		sta reg_sam_v1c
		sta reg_sam_v2c
		ldx #$0400
		ldd #$aaaa
30		std ,x++
		cmpx #$0600
		blo 30B
		; dunzip game
		puls d
		ldx #$3700
		ldu #CODE_start
		jsr [dunzip_ptr]
		jmp INIT_exec

		; initialise up to text screen
		rzb $0400-*,$80

		include "title0.s"
