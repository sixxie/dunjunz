/*

tile2bin - convert graphics to binary data suitable for Dragon/Tandy CoCo
Copyright 2014-2018 Ciaran Anscomb

Licence: GNU GPL version 3 or later <http://www.gnu.org/licenses/gpl-3.0.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

See the output of "tile2bin --help" for options.

*/

#include <ctype.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#ifdef _WIN32
#include <io.h>
#endif

#include <SDL.h>
#include <SDL_image.h>

#ifdef main
# undef main
#endif

static int getpixel(SDL_Surface *surface, int x, int y);

static void helptext(void) {
	puts(
"Usage: tile2bin [OPTION]... IMAGE-FILE\n"
"Convert graphics to binary data suitable for Dragon/Tandy CoCo.\n"
"\n"
"  -x PIXELS    offset to left edge [0]\n"
"  -y PIXELS    offset to top edge [0]\n"
"  -w PIXELS    width [from image]\n"
"  -h PIXELS    height [from image]\n"
"  -X           flip horizontally\n"
"  -Y           flip vertically\n"
"  -s SHIFT     initial right shift in pixels (0-7) [0]\n"
"  -r           resolution graphics (black & white)\n"
"  -o FILE      output filename [standard out]\n"
"\n"
"  -?, --help   show this help\n"
	);
}

static struct option long_options[] = {
	{ "help", no_argument, NULL, '?' },
	{ NULL, 0, NULL, 0 }
};

int main(int argc, char **argv) {
	int xoff = 0, yoff = 0;
	int w = 0, h = 0;
	_Bool flip_horizontal = 0;
	_Bool flip_vertical = 0;
	int shift = 0;
	_Bool resolution = 0;

#ifdef _WIN32
	setmode(fileno(stdout), O_BINARY);
#endif

	int c;
	while ((c = getopt_long(argc, argv, "x:y:w:h:XYs:ro:?",
				long_options, NULL)) != -1) {
		switch (c) {
		case 0:
			break;
		case 'x':
			xoff = strtol(optarg, NULL, 0);
			break;
		case 'y':
			yoff = strtol(optarg, NULL, 0);
			break;
		case 'w':
			w = strtol(optarg, NULL, 0);
			break;
		case 'h':
			h = strtol(optarg, NULL, 0);
			break;
		case 'X':
			flip_horizontal = 1;
			break;
		case 'Y':
			flip_vertical = 1;
			break;
		case 's':
			shift = strtol(optarg, NULL, 0);
			if (shift < 0 || shift > 7) {
				fprintf(stderr, "shift out of range\n");
				exit(EXIT_FAILURE);
			}
			break;
		case 'r':
			resolution = 1;
			break;
		case 'o':
			if (freopen(optarg, "wb", stdout) == NULL) {
				fprintf(stderr, "Couldn't open '%s' for writing: ", optarg);
				perror(NULL);
				exit(EXIT_FAILURE);
			}
			break;
		case '?':
			helptext();
			exit(EXIT_SUCCESS);
		default:
			exit(EXIT_FAILURE);
		}
	}

	if (optind >= argc) {
		fputs("no input files\n", stderr);
		exit(EXIT_FAILURE);
	}

	SDL_Surface *in = IMG_Load(argv[optind]);
	if (!in) {
		fprintf(stderr, "Couldn't read image %s: %s\n", argv[optind], IMG_GetError());
		return 1;
	}

	if (!w)
		w = in->w;
	if (!h)
		h = in->h;
	int wbytes = (w + shift + 7) >> 3;  // bytes per line
	int nbytes = wbytes * h;  // total bytes

	uint8_t *data = malloc(nbytes);
	uint8_t *datap = data;

	for (int y = 0; y < h; y++) {
		uint8_t m = 0xff;
		uint8_t d = 0;
		int n = shift;
		for (int x = 0; x < w; x++) {
			m <<= 1;
			d <<= 1;
			int getx = flip_horizontal ? (xoff + w - x - 1) : (xoff + x);
			int gety = flip_vertical ? (yoff + h - y - 1) : (yoff + y);
			int c = getpixel(in, getx, gety);
			if (resolution && c == 5) {
				c = 3;
			} else if (c == 7) {
				c = ((y*2+x)&2) ? 2 : 3;
			} else if (c <= 0) {
				c = 0;
			} else {
				c = c - 1;
			}
			if (!(n & 1)) {
				d |= (c >> 1) & 1;
			} else {
				d |= (c & 1);
			}
			n++;
			if (n == 8) {
				n = 0;
				*(datap++) = d;
			}
		}
		if (n) {
			while (n < 8) {
				d <<= 1;
				m = (m << 1) | 1;
				n++;
			}
			*(datap++) = d;
		}
	}

	SDL_FreeSurface(in);
	datap = data;

	// emit h records of wbytes bytes
	fwrite(datap, wbytes, h, stdout);
	return EXIT_SUCCESS;
}

/* black, blue, green, cyan, red, magenta, yellow, white, orange */
static int colour_map[9] = {
	0, 3, 1, 6, 4, 7, 2, 5, 8
};

static int getpixel(SDL_Surface *surface, int x, int y) {
	if (x < 0 || y < 0 || x >= surface->w || y >= surface->h)
		return -1;
	int bpp = surface->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
	Uint32 dp;
	switch(bpp) {
	case 1: dp = *p; break;
	case 2: dp = *(Uint16 *)p; break;
	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
			dp = p[0] << 16 | p[1] << 8 | p[2];
		else
			dp = p[0] | p[1] << 8 | p[2] << 16;
		break;
	case 4: dp = *(Uint32 *)p; break;
	default: dp = 0; break;
	}
	Uint8 dr, dg, db;
	SDL_GetRGB(dp, surface->format, &dr, &dg, &db);
	int colour = (dr >= 128) ? 4 : 0;
	colour |= (dg >= 128) ? 2 : 0;
	colour |= (db >= 128) ? 1 : 0;
	/* special case for orange: */
	if (colour == 6 && dg < 224) colour = 8;
	/* index into colour_map: */
	return colour_map[colour];
}
